# Pull base image

FROM andrejreznik/python-gdal:py3.10.0-gdal3.2.3

WORKDIR /code

# Install dependencies

COPY requirements.txt /code/
RUN pip install -r /code/requirements.txt

COPY . /code/


