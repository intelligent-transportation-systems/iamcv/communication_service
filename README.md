# communication_service

**This work was supported by the Austrian Science Fund (FWF), within the project ”Interaction of autonomous and manually-controlled vehicles (IAMCV)”, number P 34485-N.**

![the general structure of the communication service](doc/service_structure.jpg)

## TASK

The task of the service is to provide users with the opportunity to exchange information and interact on the road.

## HOW IS RELATED TO THE IAMCV

As part of the project, this service will be used to study the impact of additional data (for example, historical data) on decision-making algorithms. As well as how the presentation of the decision made by an autonomous car affects the behavior of the driver.

## TECHNICAL ASPECTS

The service will implement the REST protocol, so that any device with Internet access will be able to interact with the service. However, the key users of the system are road users: pedestrians, robots, cars, autonomous cars.
The service will be based on network technologies and protocols that are massively implemented and used today. However, the service itself will not be dependent on them and in the future will be able to use any accepted communication technology (C-V2X, Wi-Fi, 5G, LTE, VLC) that has access to the global network.

## ABOUT THE SERVICE IN SEVERAL PHRASES

- The demands to the interactions:
    - a receiver client has allowed communication with them;
    - a receiver client is in close area of an transmitter client
- Two clients can communicate with each other via the service if they satisfy the interaction demands
- All clients share their location data (position, speed, acc and etc.) with the service
- A client is getting real-time id information about other clients who satisfy the interaction demands
- To launch the interaction one client sends a request with the id and interaction type data of the receiver client to the server
- All clients data is anonymous
- One client can request information about another client if they satisfy the interaction demands
- Clients can request information about their environment or from it
- The Service includes three type of users: cellphones, human controlled vehicles, robots (including AV)
    - the cellphone client includes all people who use their cellphone to interact with the clients (pedestrian, bicyclist, driver etc. )

## DB structure

UML structure of the current DB

![DB:UML](doc/myapp_models.png)

Yeah I know it's too complicated. So here is a more readable representation. 

![DB:MIRO](doc/simple_db_model.jpg)

## Custom commands docs
I designd several custom commands which can simplify your life if you want to do test with the service.
Here is you can find the documentation how to use them.

[custom commands](doc/Commands/commands.md)

## EndPoint docs

You can find the complete documentation about interaction with the service here
```commandline
  /redoc
```
to enter there you need to run the service and enter to "<your ip>/redoc".
If you run it locally I'm pretty sure it will be "localhost/redoc"

## You can find here more support materials  

[MIRO](https://miro.com/app/board/o9J_lzDNtt0=/?invite_link_id=471058718220)

## How to run the service 

Before run the command s you need to add .env files into the config/project_env directory
here is a template for the .env file

```commandline
# DJANGO
export DJANGO_SECRET_KEY="django-insecure-rgct!r94j&b2jcvjo2ski0(e!2v_u9js-oe^@%--vr=hyr!km="
export ALLOWED_HOSTS='127.0.0.1'
export DEBUG=1


# DBMS
export DB_NAME=postgres
export DB_USER=postgres
export DB_PASSWORD=postgres
export DB_HOST=localhost
export DB_PORT=5431

# REDIS
export REDIS_IP='127.0.0.1'

# RABBITMQ
export RABBITMQ_USER=rabbit
export RABBITMQ_PASS=rabbit


```

### for a development purpose

1. create virtual environment
```commandline
python3 -m venv venv
```
  
2. Under virtual env install requirements 
```commandline
(venv) pip install -r requirements.txt
```
3. source the .env file 
```commandline
source config/preoject_env/.env
```
4. run the docker-compose (it will run containers as demons)
```commandline
docker-compose up -d
```
5. go to the django project 
```commandline
cd server
```
6. make migrations to map your models with DBMS
```commandline
python manage.py makemigrations
```
7. implement migrations
```commandline
python manage.py migrate
```
8. create superuser to have access to the admin page
```commandline
python manage.py createsuperuser
```
9. run the service (it will run it on localhost:8000 
```commandline
python manage.py runserver
```
10. if you found a bug or something do not be mad on me. It is better just text me and ask :) 
you can find my contacts in the end of this readme file

### for a production purpose

1. run docker-compose
```commandline
docker-compose -f docker-compose.prod.yml up -d
```
2. enter to web docker container 
```commandline
docker-compose -f docker-compose.prod.yml exec web sh
```
3. go to the django project 
```commandline
cd server
```
4. make migrations to map your models with DBMS
```commandline
python manage.py makemigrations
```
5. implement migrations
```commandline
python manage.py migrate
```
6. create superuser to have access to the admin page
```commandline
python manage.py createsuperuser
```
7. run the service (it will run it on localhost:8000 
```commandline
python manage.py runserver
```
8. if you found a bug or something do not be mad on me. It is better just text me and ask :) 
You can find my contacts in the end of this readme file


## SEVERAL EXAMPLES OF USAGE
### Drowsiness 

[Here](https://www.youtube.com/watch?v=0-Z-cMBJgsc) you can watch video with the field test.  
[Here](doc/COM_IV2022%20RUCS_IV.pdf) you can find conf. paper about it. 

### Follow the red car

Navigation apps can use the service to request information about local route of the cars nearby a client and provide them more specific information about their local target or task.

![Sketch: follow the red car](doc/Sketch_follow_the_red_car.jpg)

### Polite tailgating

One client can ask via service another client to give them road. It's definately more polite then reduce a safety distance. 

![Sketch: tailgating](doc/Sketch_tailgating.jpg)

### Social lane changing

One client can publish request to other clients about its purpose. And the other clients can response on it. It makes interaction more clear and maybe allows to reduce amplitude of deceleration and acceleration. 

![Sketch: lane changing](doc/Sketch_lane_changing.jpg)

## Citation ##
If you use this work in your research, please cite it as follows:

```
@article{smirnov2022,  
  author={Smirnov, Nikita and Tschermuth, Sebastian and Morales-Alvarez, Walter and Olaverri-Monreal Cristina},  
  journal={IEEE Intelligent Vehicles Symposium proceedings},   
  title={Interaction of Autonomous and Manually-controlled vehicles: Implementation of Road User Communication Service},   
  year={2022}, 
  doi={}
}
```
