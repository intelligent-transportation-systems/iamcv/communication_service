# Custom Commands' documentation

to check how commands works you can use 
```commandline
python manage.py "command_name" --help 
```

## list of commands: 
1. addactions
2. addproperties
4. cleandatabase
5. clustervehicle
6. filldb
7. timeclustering
8. addvehiclemodels
9. createvehicle
10. createtrip
11. closetrip
12. addvehiclestates

to see the description of a command type in the console 
```commandline
python manage.py <command_name> --help
```

## timeclustering
command measures time that is needed to return neighbors. 
number and repeat are args for python timeit library
```commandline
python manage.py timeclustering --number <int> --repeat <int>
```

## filldb 
commands adds users, trip and states from the dataset to the db, I used it to generate data for the tests. 
You can find this command in unittests 

```commandline
python manage.py filldb --trip_number <int>
```


## clustervehicle

I used it to play with different cluster methods. It actually useless due to the fact I'm using postgis to get neighbors.
But you also can use it to run some tests. It will provide you a google map with the all vehicles and their cluster.

```commandline
python manage.py clustervehicle --cluster_type <DBSCAN or Agglomerative> --build_map <bool>
```

## cleandatabase 

remove data from all of the tables. It's usefull for tests
```commandline
python manage.py cleandatabase
```

## addactions

It gets list of action names (strings) and store them to the db's table Action.\
example: 
```commandline
python manage.py addaction action1 action2 action3
```

## addproperties

It gets list of properties names (strings) and store them to the db's table Property.\
example: 
```commandline
python manage.py addproperty prop1 prop2 prop3
```

## addvehiclemodels

It request list of models from the remote dataset and store them to the db.\
It gets in input number of models that ill be stored in the db.\
example: 
```commandline
python manage.py addvehiclemodels 10000
```

## createvehicle
it gets vehicle's option and create vehicle and store in the db.\
list of options:\
* --user_id (user pk, default first)
* --actions (vehicle's allowed actions' pk, default all)
* --properties (vehicle's allowed properties' pk, default all)
* --model (model's pk, default random)
* --plate_number (vehicle's plate number, default is random)
* --vin (vehicle's vin, default is random)
* --color (vehicle's color in form #FFFFFF, default is white)

example: 
```commandline
python manage.py createvehicle --user_id 2 --actions 1 2 3 --properties 1 2
```
 
## createtrip
it creates trip for specified user and it's vehicle\
list of options:
* --user_id (user pk, default first)
* --vehicle_id (vehicle pk number, default user's first vehicle)

example: 
```commandline
python manage.py createtrip --user_id 2 --vehicle_id 1
```

## closetrip 
it closes trip for specified user's pk

example: 
```commandline
python manage.py closetrip 1
```

## addvehiclestates
it adds vehicle-states from a dataset to the db.\ 
list of options:\
* --trip_id (Trip pk, default None)
* --dataset_trip_id (vehicle # in the dataset, default user's first vehicle)

example:
```commandline
python manage.py addvehiclestates --trip_id 1 --dataset_trip_id 2
```