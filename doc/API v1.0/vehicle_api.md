# REST API for the .../vehicle/

You can find everything what you want here: 

    /redoc/#tag/vehicle  

To go there just run the service 



allowed endpoints: 

| Endpoint     | HTTP Verb                         | args                              |
|--------------|-----------------------------------|-----------------------------------|
| create       | POST OPTIONS                      |                                   |
| <int:pk>     | GET DELETE POST PUT PATCH OPTIONS |                                   |
| list         | GET                               |                                   |
| models       | GET                               | body<int:pk>, manufacture<int:pk> |
| bodies       | GET                               |                                   |
| manufactures | GET                               |                                   |

## GET DELETE POST PUT PATCH <int:pk> 
full information you can find here 

    /redoc/#operation/vehicle_read
    
### GET: READ

path: /api/v1/vehicle/{id}  
HTTP code 200  
content type: application/json

    {
      "model": 0,
      "body": 0,
      "year": "2019-08-24",
      "color": "string",
      "plate_number": "string",
      "vin": "string",
      "allowed_actions": [
        0
      ],
      "allowed_properties": [
        0
      ]
    }

### PUT: UPDATE

full information you can find here 

    /redoc/#operation/vehicle_update

path: /api/v1/vehicle/{id}

#### Request samples

content type: application/json

    {
        "model": 0,
        "body": 0,
        "year": "2019-08-24",
        "color": "string",
        "plate_number": "string",
        "vin": "string",
        "allowed_actions": [
            0
        ],
        "allowed_properties": [
            0
        ]
    }

#### Response samples

HTTP code: 200  
content type: application/json

    {
      "model": 0,
      "body": 0,
      "year": "2019-08-24",
      "color": "string",
      "plate_number": "string",
      "vin": "string",
      "allowed_actions": [
        0
      ],
      "allowed_properties": [
        0
      ]
    }

#### PATCH: partial_update

full information you can find here 

    /redoc/#operation/vehicle_partial_update

path: /api/v1/vehicle/{id}

#### Request samples
content type: application/json

    {
      "model": 0,
      "body": 0,
      "year": "2019-08-24",
      "color": "string",
      "plate_number": "string",
      "vin": "string",
      "allowed_actions": [
        0
      ],
      "allowed_properties": [
        0
      ]
    }

#### Response samples 

HTTP code: 200  
content type: application/json 

    {
      "model": 0,
      "body": 0,
      "year": "2019-08-24",
      "color": "string",
      "plate_number": "string",
      "vin": "string",
      "allowed_actions": [
        0
      ],
      "allowed_properties": [
        0
      ]
    }

### DELETE 
full information you can find here 

    /redoc/#operation/vehicle_delete

path: /api/v1/vehicle/{id}

#### Response samples 

HTTP code: 204

## GET models (args: body <int:pk>, manufacture <int:pk>)
full information you can find here 

    /redoc/#operation/vehicle_models_list

path: /api/v1/vehicle/models

#### Response samples

HTTP code: 200  
content type: application/json 

    [
      {
        "name": "string"
      }
    ]

## GET list

full information you can find here 

    /redoc/#operation/vehicle_list_list

path: /api/v1/vehicle/list

#### Response samples

HTTP code: 200  
content type: application/json 

    [
      {
        "plate_number": "string",
        "vin": "string"
      }
    ]

## GET bodies

full information you can find here 

    /redoc/#operation/vehicle_bodies_list

path: /api/v1/vehicle/bodies

#### Response samples

HTTP code: 200  
content type: application/json 

    [
      {
        "name": "string"
      }
    ]

## GET manufactures

full information you can find here 

    /redoc/#operation/vehicle_manufactures_list

path: /api/v1/vehicle/manufactures

#### Response samples

HTTP code: 200  
content type: application/json 

    [
      {
        "name": "string"
      }
    ]

## POST create

### CREATE

path: /api/v1/vehicle/create
Response: HTTP code 201 + created object
Content type: application/json  
Json properties:   
    
    {
      "model": 0,
      "body": 0,
      "year": "2019-08-24",
      "color": "string",
      "plate_number": "string",
      "vin": "string",
      "allowed_actions": [
        0
      ],
      "allowed_properties": [
        0
      ]
    }

more detailed inforamtion you can find here  
/redoc/#operation/vehicle_create_create