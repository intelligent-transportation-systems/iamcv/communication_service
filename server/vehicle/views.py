from rest_framework import generics, permissions

from RUCS.filters import UserFilter
from RUCS.permissions import IsClient

from .models import Vehicle, VehicleBody, VehicleManufacture, VehicleModel
from .serializers import (VehicleBodySerializer, VehicleListSerializer,
                          VehicleManufactureSerializer, VehicleModelSerializer,
                          VehicleSerializer)


class VehicleDetailedView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Vehicle.objects.all()
    serializer_class = VehicleSerializer
    filterset_class = UserFilter
    permission_classes = [IsClient | permissions.IsAuthenticated]


class VehicleCreateView(generics.CreateAPIView):
    queryset = Vehicle.objects.all()
    serializer_class = VehicleSerializer

    def perform_create(self, serializer):
        serializer.save(user_id=self.request.user)


class VehiclesListView(generics.ListAPIView):
    queryset = Vehicle.objects.all()
    serializer_class = VehicleListSerializer
    filterset_class = UserFilter


class VehicleModelsListView(generics.ListAPIView):
    queryset = VehicleModel.objects.all()
    serializer_class = VehicleModelSerializer
    filterset_fields = ["manufacture", "body", "year"]


class VehicleBodyListView(generics.ListAPIView):
    queryset = VehicleBody.objects.all()
    serializer_class = VehicleBodySerializer


class VehicleManufactureListView(generics.ListAPIView):
    queryset = VehicleManufacture.objects.all()
    serializer_class = VehicleManufactureSerializer
