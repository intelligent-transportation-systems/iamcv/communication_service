from django.core.validators import RegexValidator
from rest_framework import serializers, validators

from .models import (
    Vehicle,
    VehicleBody,
    VehicleManufacture,
    VehicleModel,
    VehicleModelYear,
)


class VehicleListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vehicle
        fields = [
            "id",
            "plate_number",
            "vin",
        ]


class VehicleBodySerializer(serializers.ModelSerializer):
    class Meta:
        model = VehicleBody
        fields = ["name", "id"]


class VehicleManufactureSerializer(serializers.ModelSerializer):
    class Meta:
        model = VehicleManufacture
        fields = ["name", "id"]


class VehicleModelYearSerializer(serializers.ModelSerializer):
    class Meta:
        model = VehicleModelYear
        fields = ["year", "id"]


class VehicleModelSerializer(serializers.ModelSerializer):
    body = VehicleBodySerializer(read_only=True, many=True)
    manufacture: serializers.SlugRelatedField = serializers.SlugRelatedField(
        read_only=True, many=False, slug_field="name"
    )
    year: serializers.SlugRelatedField = serializers.SlugRelatedField(
        read_only=True, many=True, slug_field="year"
    )

    class Meta:
        model = VehicleModel
        fields = ["id", "name", "year", "body", "manufacture"]
        validators = [
            validators.UniqueTogetherValidator(
                queryset=VehicleModel.objects.all(), fields=["name", "manufacture"]
            )
        ]


class VehicleSerializer(serializers.ModelSerializer):
    def body_validation(self, body: int, model):
        if body not in model.body.all():
            raise serializers.ValidationError(
                {"body": f"model {model} does not have {body} body type"}
            )

    def year_validation(self, year: int, model):
        if year not in model.year.values_list("year", flat=True):
            raise serializers.ValidationError(
                {"year": f"model {model} does not have {year} year"}
            )

    def validate(self, data):
        if not self.instance:
            self.body_validation(data["body"], data["model"])
            self.year_validation(data["year"].year, data["model"])
        else:
            if "year" in data:
                self.year_validation(data["year"].year, self.instance.model)
            if "body" in data:
                self.body_validation(data["body"], self.instance.model)

        return super(VehicleSerializer, self).validate(data)

    plate_number = serializers.CharField(
        validators=[
            RegexValidator(
                regex=r"^[A-Z0-9]*$",
                message="plate_number includes only capital alphanumeric symbols",
                code="invalid_plate_number",
            ),
        ]
    )

    vin = serializers.CharField(
        validators=[
            RegexValidator(
                regex=r"^[A-Z0-9]{17,}$",
                message="VIN includes only capital alphanumeric symbols > 17",
                code="invalid_plate_number",
            ),
            validators.UniqueValidator(queryset=Vehicle.objects.all()),
        ]
    )

    body = serializers.PrimaryKeyRelatedField(
        queryset=VehicleBody.objects.all(), allow_null=False
    )

    class Meta:
        model = Vehicle
        fields = [
            "id",
            "model",
            "body",
            "year",
            "color",
            "plate_number",
            "vin",
            "allowed_actions",
            "allowed_properties",
        ]


class VehiclePropertyModelSerializer(serializers.ModelSerializer):
    manufacture: serializers.SlugRelatedField = serializers.SlugRelatedField(
        read_only=True, many=False, slug_field="name"
    )

    class Meta:
        model = VehicleModel
        fields = ["id", "name", "manufacture"]


class NeighborVehicleSerializer(serializers.ModelSerializer):
    model = VehiclePropertyModelSerializer()
    body = VehicleBodySerializer()

    class Meta:
        model = Vehicle
        fields = [
            # "id",
            "model",
            "body",
            "year",
            "color",
            "plate_number",
            "vin",
            "allowed_actions",
            "allowed_properties",
        ]
