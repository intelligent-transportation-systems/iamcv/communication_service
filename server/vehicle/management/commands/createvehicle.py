import logging
import random
from datetime import datetime

from django.core.management.base import BaseCommand
from django.db import transaction

from api.models import Action, Property
from user.models import CustomUser
from vehicle.models import Vehicle, VehicleModel

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


class Command(BaseCommand):
    help = "create random vehicle for given user"

    def add_arguments(self, parser):
        parser.add_argument(
            "--user_id",
            dest="user_id",
            choices=list(CustomUser.objects.values_list("id", flat=True)),
            type=int,
            help="get pk number of user",
            default=list(CustomUser.objects.values_list("id", flat=True))[0],
        )
        parser.add_argument(
            "--actions",
            dest="actions_id",
            choices=list(Action.objects.values_list("id", flat=True)),
            nargs="+",
            type=int,
            help="vehicle's allowed actions, default all",
            default=list(Action.objects.values_list("id", flat=True)),
        )
        parser.add_argument(
            "--properties",
            dest="properties_id",
            choices=list(Property.objects.values_list("id", flat=True)),
            nargs="+",
            type=int,
            help="vehicle's allowed properties, default all",
            default=list(Property.objects.values_list("id", flat=True)),
        )
        parser.add_argument(
            "--model",
            dest="model_id",
            choices=list(VehicleModel.objects.values_list("id", flat=True)),
            type=int,
            help="vehicle's model, default random",
            default=list(VehicleModel.objects.values_list("id", flat=True))[0],
        )
        parser.add_argument(
            "--plate_number",
            dest="plate-number",
            type=str,
            help="vehicle's plate number, default is random",
            default="A123AA",
        )
        parser.add_argument(
            "--vin",
            dest="vin",
            type=str,
            help="vehicle's vin, default is random",
            default=None,
        )
        parser.add_argument(
            "--color",
            dest="color",
            type=str,
            help="vehicle's color in form #FFFFFF, default white",
            default="#FFFFFF",
        )

    def get_vin(self, vin: str) -> str:
        if vin is None:
            vins_list = list(Vehicle.objects.values_list("vin", flat=True))
            if not vins_list:
                return "1HGBH41JXMN109186"

            max_vin = max(vins_list)
            serial_number = int(max_vin[-6:])
            unique_vin = max_vin[:-6] + str(serial_number + 1)

            return unique_vin

        return vin

    def handle(self, *args, **options):
        with transaction.atomic():
            if type(options["actions_id"]) == int:
                options["actions_id"] = [options["actions_id"]]

            if type(options["properties_id"]) == int:
                options["properties_id"] = [options["properties_id"]]

            user = CustomUser.objects.get(id=options["user_id"])
            vin = self.get_vin(options["vin"])
            plate_number = options["plate-number"]
            actions = Action.objects.filter(id__in=options["actions_id"])
            props = Property.objects.filter(id__in=options["properties_id"])
            model = VehicleModel.objects.get(id=options["model_id"])
            date = datetime(
                year=list(model.year.values_list("year", flat=True))[0],
                month=random.randint(1, 12),
                day=random.randint(1, 27),
            )
            body = model.body.first()

            logger.debug(
                f"{user=},{vin=},{plate_number=},{actions=},{props=},{model=},{date=},{body=}"
            )

            vehicle = Vehicle.objects.create(
                user_id=user,
                vin=vin,
                plate_number=plate_number,
                model=model,
                color=options["color"],
                year=date,
                body=body,
            )

            if not list(props):
                raise ValueError("properties may not be empty")

            if not list(actions):
                raise ValueError("actions may not be empty")

            vehicle.allowed_properties.add(*props)
            vehicle.allowed_actions.add(*actions)

            vehicle.full_clean()
