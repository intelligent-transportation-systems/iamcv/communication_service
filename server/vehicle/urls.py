from django.urls import path

from .views import (VehicleBodyListView, VehicleCreateView,
                    VehicleDetailedView, VehicleManufactureListView,
                    VehicleModelsListView, VehiclesListView)

urlpatterns = [
    path("create", VehicleCreateView.as_view(), name="vehicle-create"),
    path("<int:pk>", VehicleDetailedView.as_view(), name="vehicle-detailed"),
    path("list", VehiclesListView.as_view(), name="vehicles-list"),
    path("models", VehicleModelsListView.as_view(), name="vehicle-models-list"),
    path("bodies", VehicleBodyListView.as_view(), name="vehicle-bodies-list"),
    path(
        "manufactures",
        VehicleManufactureListView.as_view(),
        name="vehicle-manufactures-list",
    ),
]
