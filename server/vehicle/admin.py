from django.contrib import admin

from .models import Vehicle, VehicleBody, VehicleManufacture, VehicleModel

admin.site.register(Vehicle)
admin.site.register(VehicleBody)
admin.site.register(VehicleModel)
admin.site.register(VehicleManufacture)
