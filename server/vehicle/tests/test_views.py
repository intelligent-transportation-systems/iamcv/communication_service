import logging

from django.core.management import call_command
from django.test import TestCase
from django.urls import reverse

from api.models import Action, Property
from user.models import CustomUser
from vehicle.models import (
    Vehicle,
    VehicleBody,
    VehicleManufacture,
    VehicleModel,
    VehicleModelYear,
)

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

logger.info("Start Vehicle's views test!")


class ViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = CustomUser.objects.create_user(
            username="testusername", password="testpassword", email="test@test.com"
        )

        call_command("addvehiclemodels", 10)
        call_command("addactions", ["testaction1", "testaction2"])
        call_command("addproperties", ["average_speed", "average_acceleration"])

        for _ in range(3):
            call_command("createvehicle", user_id=cls.user.id)

    def setUp(self):
        self.assertTrue(
            self.client.login(username="testusername", password="testpassword")
        )

    def test_can_get_models(self):
        response = self.client.get(reverse("vehicle-models-list"))
        logger.debug(f"responce: {response.json()}")

        self.assertEqual(response.status_code, 200)

        models = VehicleModel.objects.all()
        self.assertEqual(len(response.json()), len(models))

        for check_model, resp_model in zip(models, response.json()):
            logger.debug(f'{check_model.name=} == {resp_model["name"]=}')
            self.assertEqual(check_model.name, resp_model["name"])

    def test_can_filter_models_by_body(self):
        body_filter = VehicleBody.objects.first()

        response = self.client.get(
            reverse("vehicle-models-list"), {"body": body_filter.id}
        )
        logger.debug(f"responce: {response.json()}")

        self.assertEqual(response.status_code, 200)

        expected_models = VehicleModel.objects.filter(body=body_filter)
        self.assertEqual(len(response.json()), len(expected_models))

        for check_model, resp_model in zip(expected_models, response.json()):
            logger.debug(f'{check_model.name=} == {resp_model["name"]=}')
            self.assertEqual(check_model.name, resp_model["name"])

    def test_can_filter_models_by_year(self):
        year_filter = VehicleModelYear.objects.first()

        response = self.client.get(
            reverse("vehicle-models-list"), {"year": year_filter.id}
        )
        logger.debug(f"responce: {response.json()}")

        self.assertEqual(response.status_code, 200)

        expected_models = VehicleModel.objects.filter(year=year_filter)
        self.assertEqual(len(response.json()), len(expected_models))

        for check_model, resp_model in zip(expected_models, response.json()):
            logger.debug(f'{check_model.name=} == {resp_model["name"]=}')
            self.assertEqual(check_model.name, resp_model["name"])

    def test_can_filter_models_by_manufacture(self):
        manufacture_filter = VehicleManufacture.objects.first()

        response = self.client.get(
            reverse("vehicle-models-list"), {"manufacture": manufacture_filter.id}
        )
        logger.debug(f"responce: {response.json()}")

        self.assertEqual(response.status_code, 200)

        expected_models = VehicleModel.objects.filter(manufacture=manufacture_filter)
        self.assertEqual(len(response.json()), len(expected_models))

        for check_model, resp_model in zip(expected_models, response.json()):
            logger.debug(f'{check_model.name=} == {resp_model["name"]=}')
            self.assertEqual(check_model.name, resp_model["name"])

    def test_can_get_bodies(self):
        logger.info("start test_can_get_bodies")

        response = self.client.get(reverse("vehicle-bodies-list"))
        logger.debug(f"responce: {response.json()}")

        self.assertEqual(response.status_code, 200)

        bodies = VehicleBody.objects.all()
        self.assertEqual(len(response.json()), len(bodies))

        for check_body, resp_model in zip(bodies, response.json()):
            logger.debug(f'{check_body.name=} == {resp_model["name"]=}')
            self.assertEqual(check_body.name, resp_model["name"])

    def test_can_get_manufactures(self):
        logger.info("start test_can_get_manufactures")

        response = self.client.get(reverse("vehicle-manufactures-list"))
        logger.debug(f"responce: {response.json()}")

        self.assertEqual(response.status_code, 200)
        manufactures = VehicleManufacture.objects.all()

        self.assertEqual(len(response.json()), len(manufactures))

        for check_manufacture, resp_model in zip(manufactures, response.json()):
            logger.debug(f'{check_manufacture.name=} == {resp_model["name"]=}')
            self.assertEqual(check_manufacture.name, resp_model["name"])

    def test_can_get_vehicle_list(self):
        logger.info("start test_can_get_vehicle_list")

        response = self.client.get(reverse("vehicles-list"))
        logger.debug(f"responce: {response.json()}")

        self.assertEqual(response.status_code, 200)

        vehicles = Vehicle.objects.all()
        self.assertEqual(len(response.json()), len(vehicles))

        for check_vehicle, resp_model in zip(vehicles, response.json()):
            logger.debug(f'{check_vehicle.vin=} == {resp_model["vin"]=}')
            self.assertEqual(check_vehicle.vin, resp_model["vin"])

    def test_can_create_vehicle(self):
        logger.info("start test_can_create_vehicle")

        allowed_actions = list(Action.objects.values_list("id", flat=True))
        allowed_properties = list(Property.objects.values_list("id", flat=True))
        model = VehicleModel.objects.first()
        year = model.year.values_list("year", flat=True)[0]
        body = model.body.values_list("id", flat=True)[0]
        vehicle = {
            "model": model.id,
            "year": f"{year}-05-06",
            "color": "#FF00FF",
            "body": body,
            "plate_number": "DA123FF",
            "vin": "1HGBH41JXMN109180",
            "allowed_actions": allowed_actions,
            "allowed_properties": allowed_properties,
        }

        response = self.client.post(reverse("vehicle-create"), data=vehicle)
        logger.debug(f"responce: {response.json()}")

        self.assertEqual(response.status_code, 201)

        created_vehicle = Vehicle.objects.get(vin=vehicle["vin"])

        logger.debug(f'{created_vehicle.vin=} == {vehicle["vin"]=}')
        self.assertEqual(created_vehicle.vin, vehicle["vin"])

    def test_can_get_vehicle(self):
        logger.info("start test_can_create_vehicle")
        vehicle = Vehicle.objects.filter(user_id=self.user).first()

        response = self.client.get(
            reverse("vehicle-detailed", kwargs={"pk": vehicle.id})
        )
        logger.debug(f"responce: {response.json()}")

        self.assertEqual(response.status_code, 200)

        logger.debug(f"{vehicle=} == {response.json()=}")
        self.assertEqual(vehicle.vin, response.json()["vin"])

    def test_can_update_vehicle(self):
        logger.info("start test_can_update_vehicle")
        vehicle = Vehicle.objects.filter(user_id=self.user).first()

        response = self.client.patch(
            reverse("vehicle-detailed", kwargs={"pk": vehicle.id}),
            {"plate_number": "AA240CDB"},
            content_type="application/json",
        )
        logger.debug(f"responce: {response.json()}")

        self.assertEqual(response.status_code, 200)
        vehicle = Vehicle.objects.get(id=vehicle.id)

        logger.debug(f'{vehicle.plate_number=} == {response.json()["plate_number"]=}')
        self.assertEqual(vehicle.plate_number, response.json()["plate_number"])

    def test_can_del_vehicle(self):
        logger.info("start test_can_del_vehicle")
        vehicle = Vehicle.objects.filter(user_id=self.user).first()

        response = self.client.delete(
            reverse("vehicle-detailed", kwargs={"pk": vehicle.id})
        )

        self.assertEqual(response.status_code, 204)
        self.assertTrue(not Vehicle.objects.filter(id=vehicle.id).exists())

    def test_cannot_add_wrong_body(self):
        vehicle = Vehicle.objects.filter(user_id=self.user).first()
        body = VehicleBody.objects.exclude(id__in=vehicle.model.body.all()).first()

        response = self.client.patch(
            reverse("vehicle-detailed", kwargs={"pk": vehicle.id}),
            {"body": body.id},
            content_type="application/json",
        )

        logger.debug(f"responce: {response.json()}")
        self.assertEqual(response.status_code, 400)
        self.assertTrue("body" in response.json())

    def test_cannot_add_wrong_year(self):
        vehicle = Vehicle.objects.filter(user_id=self.user).first()
        year = VehicleModelYear.objects.exclude(id__in=vehicle.model.year.all()).first()

        response = self.client.patch(
            reverse("vehicle-detailed", kwargs={"pk": vehicle.id}),
            {"year": f"{year}-10-20"},
            content_type="application/json",
        )

        logger.debug(f"responce: {response.json()}")
        self.assertEqual(response.status_code, 400)
        self.assertTrue("year" in response.json())

    def test_cannot_add_wrong_plate_number(self):
        vehicle = Vehicle.objects.filter(user_id=self.user).first()

        response = self.client.patch(
            reverse("vehicle-detailed", kwargs={"pk": vehicle.id}),
            {"plate_number": "aa 123 bc"},
            content_type="application/json",
        )
        logger.debug(f"responce: {response.json()}")
        self.assertEqual(response.status_code, 400)
        self.assertTrue("plate_number" in response.json())

    def test_cannot_add_wrong_vin(self):
        vehicle = Vehicle.objects.filter(user_id=self.user).first()

        response = self.client.patch(
            reverse("vehicle-detailed", kwargs={"pk": vehicle.id}),
            {"vin": "asdasdasd123asd"},
            content_type="application/json",
        )
        logger.debug(f"responce: {response.json()}")
        self.assertEqual(response.status_code, 400)
        self.assertTrue("vin" in response.json())

    def test_cannot_create_vehicle_with_not_unique_vin(self):
        vehicle = Vehicle.objects.filter(user_id=self.user).first()
        allowed_actions = list(Action.objects.values_list("id", flat=True))
        allowed_properties = list(Property.objects.values_list("id", flat=True))

        vehicle = {
            "model": vehicle.model.id,
            "year": f"{vehicle.model.year.first()}-05-06",
            "color": "#FF00FF",
            "body": vehicle.model.body.first().id,
            "plate_number": "DA123FF",
            "vin": vehicle.vin,
            "allowed_actions": allowed_actions,
            "allowed_properties": allowed_properties,
        }

        response = self.client.post(reverse("vehicle-create"), data=vehicle)

        logger.debug(f"responce: {response.json()}")
        self.assertEqual(response.status_code, 400)
        self.assertTrue("vin" in response.json())
