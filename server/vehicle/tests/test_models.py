from datetime import timedelta

from django.core.exceptions import ValidationError
from django.db.utils import IntegrityError
from django.test import TransactionTestCase
from django.utils import timezone

from user.models import CustomUser
from vehicle.models import (
    Vehicle,
    VehicleBody,
    VehicleManufacture,
    VehicleModel,
    VehicleModelYear,
)


class VehicleManufactureModelTest(TransactionTestCase):
    def test_unique_name(self):
        with self.assertRaises(IntegrityError):
            VehicleManufacture.objects.create(name="test")
            VehicleManufacture.objects.create(name="test")


class VehicleBodyModelTest(TransactionTestCase):
    def test_unique_name(self):
        with self.assertRaises(IntegrityError):
            VehicleBody.objects.create(name="test")
            VehicleBody.objects.create(name="test")


class VehicleModelYearTest(TransactionTestCase):
    def test_min_year_value(self):
        with self.assertRaises(ValidationError):
            vehicle_model_year = VehicleModelYear(year=1800)
            vehicle_model_year.full_clean()
            vehicle_model_year.save()

    def test_unique_name(self):
        with self.assertRaises(IntegrityError):
            VehicleModelYear.objects.create(year=2020)
            VehicleModelYear.objects.create(year=2020)


class VehicleModelModelTest(TransactionTestCase):
    # def tearDown(self):
    #     call_command("cleandatabase")

    def test_unique_model(self):
        with self.assertRaises(IntegrityError):
            VehicleModel.objects.create(
                name="c40",
                manufacture=VehicleManufacture.objects.create(name="Volvo"),
            )
            VehicleModel.objects.create(
                name="c40",
                manufacture=VehicleManufacture.objects.create(name="Volvo"),
            )


class VehicleModelTest(TransactionTestCase):
    # def tearDown(self):
    #     call_command("cleandatabase")

    def setUp(self):
        self.user = CustomUser.objects.create(username="test", password="password")
        self.body = VehicleBody.objects.create(name="sedan")
        self.year = VehicleModelYear.objects.create(year=2010)
        self.model = VehicleModel.objects.create(
            name="c40",
            manufacture=VehicleManufacture.objects.create(name="Volvo"),
        )
        self.model.body.add(self.body)
        self.model.year.add(self.year)

    def create_vehicle(self, **kwargs):
        return Vehicle(
            user_id=kwargs.get("user_id", self.user),
            model=kwargs.get("model", self.model),
            body=kwargs.get("body", self.body),
            color=kwargs.get("color", "#FFFFFF"),
            plate_number=kwargs.get("plate_number", "A123AA"),
            vin=kwargs.get("vin", "1HGBH41JXMN109186"),
            year=kwargs.get("year", "2010-06-10"),
        )

    def test_can_write_correct_data(self):
        vin = "1HGBH41JXMN109186"
        vehicle = self.create_vehicle()
        vehicle.full_clean()
        vehicle.save()

        self.assertEqual(vehicle, Vehicle.objects.get(vin=vin))

    def test_plate_number_contains_only_capital_alphanumeric_symbols(self):
        vehicle = self.create_vehicle(plate_number="a123aa")

        with self.assertRaises(ValidationError):
            vehicle.full_clean()

    def test_vin_contains_only_capital_symbols_more_17_occurrences(self):
        vehicle = self.create_vehicle(vin="1HGBH41JxMN109186")

        with self.assertRaises(ValidationError):
            vehicle.full_clean()

    def test_vin_is_unique(self):
        vin = "1HGBH41JXMN109186"
        vehicle = self.create_vehicle(vin=vin)
        vehicle.full_clean()
        vehicle.save()

        vehicle = self.create_vehicle(vin=vin)

        with self.assertRaises((IntegrityError, ValidationError)):
            vehicle.full_clean()
            vehicle.save()

    def test_years_not_be_grater_than_today(self):
        vehicle = self.create_vehicle(
            year=str(timezone.now().date() + timedelta(days=1)),
        )

        with self.assertRaises(IntegrityError):
            vehicle.save()

    def test_correct_combination_model_body(self):
        body = VehicleBody.objects.create(name="coupe")
        vehicle = self.create_vehicle(body=body)

        with self.assertRaises(ValidationError):
            vehicle.full_clean()
            vehicle.save()

    def test_correct_combination_model_year(self):
        vehicle = self.create_vehicle(year="2009-05-05")

        with self.assertRaises(ValidationError):
            vehicle.full_clean()
            vehicle.save()
