import json

import requests
from django.core.exceptions import ValidationError
from django.core.management import call_command
from django.test import TestCase

from api.models import Action, Property
from user.models import CustomUser
from vehicle.models import (
    Vehicle,
    VehicleBody,
    VehicleManufacture,
    VehicleModel,
    VehicleModelYear,
)


class AddVehicleModelsCommandTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.limit = 10
        cls.url = f"https://parseapi.back4app.com/classes/Carmodels_Car_Model_List?order=Year&limit={cls.limit}"
        cls.headers = {
            "X-Parse-Application-Id": "v0mZ5eDsC6ltQlp634YY6g8QrhYLSGteFNIBN8Uy",
            "X-Parse-REST-API-Key": "9TRyYtzfPcJHtygxiL1mv6xkGGVBriHfByq3eBup",
        }
        cls.years = set()
        cls.manufactures = set()
        cls.bodies = set()
        cls.models = set()

        cls.data = json.loads(
            requests.get(cls.url, headers=cls.headers).content.decode("utf-8")
        )

    def test_correct_storing_db(self):
        call_command("addvehiclemodels", self.limit)

        for result in self.data["results"]:
            self.models.add(result["Model"])
            self.years.add(result["Year"])
            self.manufactures.add(result["Make"].strip())
            self.bodies.update([s.strip() for s in result["Category"].split(",")])

        years = list(VehicleModelYear.objects.values_list("year", flat=True))
        bodies = list(VehicleBody.objects.values_list("name", flat=True))
        manufactures = list(VehicleManufacture.objects.values_list("name", flat=True))
        models = list(VehicleModel.objects.values_list("name", flat=True))

        expected_years = list(self.years)
        expected_bodies = list(self.bodies)
        expected_manufactures = list(self.manufactures)
        expected_models = list(self.models)

        self.assertEqual(years.sort(), expected_years.sort())
        self.assertEqual(bodies.sort(), expected_bodies.sort())
        self.assertEqual(manufactures.sort(), expected_manufactures.sort())
        self.assertEqual(models.sort(), expected_models.sort())


class CreateVehicleCommandTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        call_command("addvehiclemodels", 2)
        call_command("addactions", ["action1", "action2"])
        call_command("addproperties", ["prop1", "prop2"])

        cls.user = CustomUser.objects.create_user(
            username="testuser", password="testpassword"
        )
        cls.user2 = CustomUser.objects.create_user(
            username="testuser2", password="testpassword"
        )

    def test_without_options(self):
        call_command("createvehicle")
        vehicle = Vehicle.objects.first()
        # checking the default values
        self.assertEqual(vehicle.vin, "1HGBH41JXMN109186")

        self.assertEqual(vehicle.plate_number, "A123AA")
        self.assertEqual(vehicle.color, "#FFFFFF")

    def test_user_option(self):
        call_command("createvehicle", user_id=self.user.id)
        call_command("createvehicle", user_id=self.user2.id)
        vehicle = self.user2.vehicle_set.first()
        self.assertEqual(vehicle.user_id, self.user2)

    def test_cannot_add_wrong_user_option(self):
        with self.assertRaises(CustomUser.DoesNotExist):
            call_command("createvehicle", user_id=10)

    def test_actions_option(self):
        call_command(
            "createvehicle",
            actions=list(Action.objects.values_list("id", flat=True))[0],
        )
        vehicle = Vehicle.objects.first()
        self.assertEqual(vehicle.allowed_actions.count(), 1)

    def test_cannot_add_wrong_actions_option(self):
        with self.assertRaises(ValueError):
            call_command(
                "createvehicle",
                actions=5,
            )

        self.assertEqual(Vehicle.objects.all().count(), 0)

    def test_properties_option(self):
        call_command(
            "createvehicle",
            properties=list(Property.objects.values_list("id", flat=True))[0],
        )
        vehicle = Vehicle.objects.first()
        self.assertEqual(vehicle.allowed_properties.count(), 1)

    def test_cannot_add_wrong_properties_option(self):
        with self.assertRaises(ValueError):
            call_command(
                "createvehicle",
                properties=5,
            )

        self.assertEqual(Vehicle.objects.all().count(), 0)

    def test_model_option(self):
        model_id = list(VehicleModel.objects.values_list("id", flat=True))[1]
        call_command(
            "createvehicle",
            model_id=model_id,
        )
        vehicle = Vehicle.objects.first()
        self.assertEqual(vehicle.model, VehicleModel.objects.get(id=model_id))

    def test_cannot_add_wrong_model_option(self):
        with self.assertRaises(VehicleModel.DoesNotExist):
            call_command(
                "createvehicle",
                model_id=5,
            )

    def test_plate_number_option(self):
        plate_number = "BB123CAC"
        call_command(
            "createvehicle",
            plate_number=plate_number,
        )
        vehicle = Vehicle.objects.first()
        self.assertEqual(vehicle.plate_number, plate_number)

    def test_cannot_add_wrong_plate_number_option(self):
        with self.assertRaises(ValidationError):
            call_command(
                "createvehicle",
                plate_number="654asdasd45asd564",
            )

    def test_vin_option(self):
        vin = "1HGBH41XXXN109186"
        call_command(
            "createvehicle",
            vin=vin,
        )
        vehicle = Vehicle.objects.first()
        self.assertEqual(vehicle.vin, vin)

    def test_cannot_add_wrong_vin_option(self):
        with self.assertRaises(ValidationError):
            call_command(
                "createvehicle",
                vin="654asdasd45asd564",
            )

    def test_color_option(self):
        color = "#FFAAFF"
        call_command(
            "createvehicle",
            color=color,
        )
        vehicle = Vehicle.objects.first()
        self.assertEqual(vehicle.color, color)

    def test_cannot_add_wrong_color_option(self):
        with self.assertRaises(ValidationError):
            call_command(
                "createvehicle",
                color="red",
            )
