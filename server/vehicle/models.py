from colorfield.fields import ColorField
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator, RegexValidator
from django.db import models
from django.utils import timezone

from api.models import Action, Property
from user.models import CustomUser


class VehicleManufacture(models.Model):
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=["name"], name="unique_manufacture_name")
        ]


class VehicleBody(models.Model):
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=["name"], name="unique_body_name")
        ]


class VehicleModelYear(models.Model):
    year = models.IntegerField(
        unique=True, validators=[MinValueValidator(limit_value=1920)]
    )

    def __str__(self):
        return str(self.year)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=["year"], name="unique_model_year")
        ]


class VehicleModel(models.Model):
    name = models.CharField(max_length=50)
    manufacture = models.ForeignKey(VehicleManufacture, on_delete=models.CASCADE)
    body = models.ManyToManyField(VehicleBody)
    year = models.ManyToManyField(VehicleModelYear)

    def __str__(self):
        return f"{self.manufacture.name} {self.name}"

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=["name", "manufacture"], name="unique_model")
        ]


class Vehicle(models.Model):
    user_id = models.ForeignKey(CustomUser, on_delete=models.CASCADE)

    allowed_actions = models.ManyToManyField(Action)
    allowed_properties = models.ManyToManyField(Property)

    model = models.ForeignKey(VehicleModel, null=True, on_delete=models.SET_NULL)
    body = models.ForeignKey(
        VehicleBody,
        null=True,
        blank=False,
        on_delete=models.SET_NULL,
    )

    color = ColorField()

    plate_number = models.CharField(
        max_length=20,
        blank=False,
        validators=[
            RegexValidator(
                regex=r"^[A-Z0-9]*$",
                message="plate_number includes only capital alphanumeric symbols",
                code="invalid_plate_number",
            ),
        ],
    )

    year = models.DateField()

    vin = models.CharField(
        max_length=20,
        unique=True,
        validators=[
            RegexValidator(
                regex=r"^[A-Z0-9]{17,}$",
                message="VIN includes only capital alphanumeric symbols > 17",
                code="invalid_plate_number",
            ),
        ],
    )

    def clean(self):
        if self.body not in self.model.body.all():
            raise ValidationError(
                f"model {self.model} does not have {self.body} body type"
            )

        if self.year.year not in self.model.year.values_list("year", flat=True):
            raise ValidationError(f"model {self.model} does not have {self.year} year")

    def __str__(self):
        return f"{self.vin}"

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=["vin"], name="unique_vin"),
            models.CheckConstraint(
                name="check_year_gte_today",
                check=models.Q(year__lte=timezone.now().date()),
            ),
        ]
