from django_filters import FilterSet


class UserFilter(FilterSet):
    def filter_queryset(self, queryset):
        queryset = queryset.filter(user_id=self.request.user)
        return super(UserFilter, self).filter_queryset(queryset)
