import os
from pathlib import Path

import pandas as pd


def get_dataset() -> pd.DataFrame:
    current_dir = Path(os.path.dirname(__file__))
    dataset_path = current_dir.joinpath("vehicle_data.csv")

    return pd.read_csv(dataset_path)


if __name__ == "__main__":
    get_dataset()
