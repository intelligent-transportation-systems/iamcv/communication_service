import json

import pika

credentials = pika.credentials.PlainCredentials(
    username="rabbit",
    password="rabbit",
)
connection_parameters = pika.ConnectionParameters(
    host="localhost",
    port=5672,
    heartbeat=600,
    blocked_connection_timeout=100,
    credentials=credentials,
)

connection = pika.BlockingConnection(connection_parameters)

channel = connection.channel()

channel.queue_declare(queue="test")


def callback(ch, method, properties, body):
    print(f"{ch=}\n{method=}\n{properties}\n{body=}")

    data = json.loads(body)
    print(data)


channel.basic_consume(
    queue="test",
    on_message_callback=callback,
    auto_ack=True,
)

if __name__ == "__main__":
    channel.start_consuming()
