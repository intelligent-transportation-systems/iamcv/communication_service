import json

import pika

credentials = pika.credentials.PlainCredentials(
    username="rabbit",
    password="rabbit",
)

connection_parameters = pika.ConnectionParameters(
    host="localhost",
    port=5672,
    heartbeat=600,
    blocked_connection_timeout=100,
    credentials=credentials,
)

connection = pika.BlockingConnection(connection_parameters)

channel = connection.channel()


def publish(method: str, body: dict):
    properties = pika.BasicProperties(method)
    channel.basic_publish(
        exchange="",
        routing_key="test",
        body=json.dumps(body),
        properties=properties,
    )


if __name__ == "__main__":
    publish("test_method", {"test_data": "hello_world"})
