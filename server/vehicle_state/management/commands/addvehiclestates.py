import random
from datetime import datetime

import pandas as pd
from django.core.management import BaseCommand
from django.db import transaction

from RUCS.vehicle_state_data.dataset import get_dataset
from trip.models import Trip
from vehicle_state.serializers import VehicleStateSerializer


class Command(BaseCommand):
    def add_arguments(self, parser):
        dataset = self._get_dataset()

        parser.add_argument(
            "--trip_id",
            choices=list(Trip.objects.values_list("id", flat=True)),
            type=int,
            help="pk number of a trip",
        )
        parser.add_argument(
            "--dataset_trip_id",
            choices=dataset["vehicle_id"].unique(),
            type=int,
            help="vehicle number from dataset",
        )

    def _get_dataset(self) -> pd.DataFrame:
        return get_dataset()

    def _get_vehicle_dataframe(self, dataset_trip_id: int) -> pd.DataFrame:
        dataset = self._get_dataset()

        return dataset[dataset["vehicle_id"] == dataset_trip_id]

    def handle(self, *args, **options):
        with transaction.atomic():
            trip_id = options["dataset_trip_id"]
            vehicles_states: pd.DataFrame = self._get_vehicle_dataframe(trip_id)

            if vehicles_states.empty:
                raise ValueError(f"there is not {trip_id} vehicle in the dataset")

            for row_idx, row_series in vehicles_states.iterrows():

                vehicle_state_serializer = VehicleStateSerializer(
                    partial=True,
                    data={
                        "time_stamp": datetime.fromtimestamp(
                            row_series["timestep_time"]
                        ),
                        "trip_id": options["trip_id"],
                        "location": {
                            "heading_angle": row_series["vehicle_angle"],
                            "acceleration": row_series["vehicle_acceleration"],
                            "speed": row_series["vehicle_speed"],
                            "position": f"POINT({row_series['vehicle_x']} {row_series['vehicle_y']})",
                            "accuracy": 80,
                        },
                        "driver": {
                            "concentrated_on_the_road": random.choice([True, False])
                        },
                    },
                )

                vehicle_state_serializer.is_valid(raise_exception=True)
                vehicle_state_serializer.save()
