from rest_framework import generics

from trip.models import Trip

from .filters import VehicleStateFilter
from .models import VehicleState
from .serializers import VehicleStateSerializer


class CreateVehicleState(generics.CreateAPIView):
    queryset = VehicleState.objects.all()
    serializer_class = VehicleStateSerializer

    def post(self, request, *args, **kwargs):
        trip = Trip.objects.filter(user_id=self.request.user).latest("start_at")
        request.data["trip_id"] = trip.id

        return super(CreateVehicleState, self).post(request, *args, **kwargs)


class ListVehicleState(generics.ListAPIView):
    queryset = VehicleState.objects.all()
    serializer_class = VehicleStateSerializer
    # it has wrong type definition in the base class, so I just ignore the type error
    filter_backends = [VehicleStateFilter]  # type: ignore[list-item]
    filterset_fields = ["trip_id"]
