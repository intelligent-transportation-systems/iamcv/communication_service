from django_filters.rest_framework import DjangoFilterBackend


class VehicleStateFilter(DjangoFilterBackend):
    def filter_queryset(self, request, queryset, view):
        queryset = queryset.filter(trip_id__user_id=request.user.id)
        return super(VehicleStateFilter, self).filter_queryset(request, queryset, view)
