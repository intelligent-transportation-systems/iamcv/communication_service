from django.contrib import admin

from .models import ControlState, EngineState, LocationState, VehicleState

admin.site.register(VehicleState)
admin.site.register(LocationState)
admin.site.register(EngineState)
admin.site.register(ControlState)
