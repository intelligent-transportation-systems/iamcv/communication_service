from django.db.utils import IntegrityError
from django.test import TestCase

from vehicle_state.models import ControlState, EngineState, LocationState


class EngineStateModelTest(TestCase):
    def create_engine_state(self, **kwargs) -> EngineState:
        return EngineState(
            rpm=kwargs.get("rpm", 2100.3),
            power_consumption=kwargs.get("power_consumption", 50.5),
            battery_charge=kwargs.get("battery_charge", 85.5),
        )

    def test_can_write_correct_data(self):
        engine_state = self.create_engine_state()
        engine_state.save()
        self.assertEqual(engine_state, EngineState.objects.get(rpm=2100.3))

    def test_rpm_is_more_than_0(self):
        engine_state = self.create_engine_state(rpm=-2)
        with self.assertRaises(IntegrityError):
            engine_state.save()

    def test_battery_charge_is_less_than_100(self):
        engine_state = self.create_engine_state(battery_charge=102)
        with self.assertRaises(IntegrityError):
            engine_state.save()

    def test_battery_charge_is_more_than_0(self):
        engine_state = self.create_engine_state(battery_charge=-2)
        with self.assertRaises(IntegrityError):
            engine_state.save()


class ControlStateModelTest(TestCase):
    def create_control_state(self, **kwargs) -> ControlState:
        return ControlState(
            steering_wheel_angle=kwargs.get("steering_angle", 100.3),
            gas_pedal_value=kwargs.get("gas_value", 50.5),
            stop_pedal_value=kwargs.get("stop_value", 20),
        )

    def test_can_write_correct_data(self):
        control_state = self.create_control_state()
        control_state.save()
        self.assertEquals(control_state, ControlState.objects.get(gas_pedal_value=50.5))

    def test_steering_angle_is_less_than_720(self):
        control_state = self.create_control_state(steering_angle=760)
        with self.assertRaises(IntegrityError):
            control_state.save()

    def test_steering_angle_is_bigger_than_m720(self):
        control_state = self.create_control_state(steering_angle=-760)
        with self.assertRaises(IntegrityError):
            control_state.save()

    def test_gas_value_is_less_than_100(self):
        control_state = self.create_control_state(gas_value=102)
        with self.assertRaises(IntegrityError):
            control_state.save()

    def test_gas_value_is_bigger_than_0(self):
        control_state = self.create_control_state(gas_value=-2)
        with self.assertRaises(IntegrityError):
            control_state.save()

    def test_stop_value_is_less_than_100(self):
        control_state = self.create_control_state(stop_value=102)
        with self.assertRaises(IntegrityError):
            control_state.save()

    def test_stop_value_is_bigger_than_0(self):
        control_state = self.create_control_state(stop_value=-2)
        with self.assertRaises(IntegrityError):
            control_state.save()


class LocationStateModelTest(TestCase):
    def create_location_state(self, **kwargs) -> LocationState:
        return LocationState(
            heading_angle=kwargs.get("heading_angle", 10.5),
            acceleration=kwargs.get("acceleration", 10),
            speed=kwargs.get("speed", 30),
            position=kwargs.get("position", "POINT(45.5556 56.5556)"),
            accuracy=kwargs.get("accuracy", 45.5),
            road_name=kwargs.get("road_name", "test_road"),
        )

    def test_can_write_correct_data(self):
        location = self.create_location_state()
        location.save()
        self.assertEqual(location, LocationState.objects.first())

    def test_heading_angle_is_less_than_360(self):
        location = self.create_location_state(heading_angle=362.0)
        with self.assertRaises(IntegrityError):
            location.save()

    def test_heading_angle_is_bigger_than_0(self):
        location = self.create_location_state(heading_angle=-2)
        with self.assertRaises(IntegrityError):
            location.save()

    def test_accuracy_is_less_than_100(self):
        location = self.create_location_state(accuracy=362.0)
        with self.assertRaises(IntegrityError):
            location.save()

    def test_accuracy_is_bigger_than_0(self):
        location = self.create_location_state(accuracy=-182)
        with self.assertRaises(IntegrityError):
            location.save()
