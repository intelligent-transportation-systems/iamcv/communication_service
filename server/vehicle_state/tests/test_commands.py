import logging
import os

import pandas as pd
from django.core.management import call_command
from django.test import TestCase
from rest_framework.serializers import ValidationError

from trip.models import Trip
from user.models import CustomUser
from vehicle_state.models import VehicleState

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


class AddVehicleStatesCommandTest(TestCase):
    @staticmethod
    def get_data_set_path():
        test_dir_path = os.path.dirname(__file__)
        vehicle_state_app_path = os.path.dirname(test_dir_path)
        project_dir_path = os.path.dirname(vehicle_state_app_path)
        return os.path.join(
            project_dir_path, "RUCS/vehicle_state_data/vehicle_data.csv"
        )

    @classmethod
    def setUpTestData(cls):
        dataset_path = cls.get_data_set_path()
        cls.vehicles_states = pd.read_csv(dataset_path)

        cls.user = CustomUser.objects.create_user(
            username="testuser", password="testpassword"
        )

        cls.user2 = CustomUser.objects.create_user(
            username="testuser2", password="testpassword"
        )

        call_command("addactions", ["testaction1", "testaction2"])
        call_command("addproperties", ["testproperty1", "testproperty2"])
        call_command("addvehiclemodels", 2)

        call_command("createvehicle", user_id=cls.user.id)
        call_command("createvehicle", user_id=cls.user2.id)

        call_command("createtrip", user_id=cls.user.id)
        call_command("createtrip", user_id=cls.user2.id)

    def compare_results(
        self, expected_states: pd.DataFrame, given_states: VehicleState
    ):
        self.assertEqual(len(expected_states), len(given_states))

        for given_state in given_states:
            logger.debug(given_state.time_stamp.timestamp())

            state = expected_states[
                expected_states["timestep_time"] == given_state.time_stamp.timestamp()
            ]

            logger.debug(
                f"{state['vehicle_acceleration'].values[0]} == {given_state.location.acceleration}"
            )
            logger.debug(
                f"{state['vehicle_angle'].values[0]} == {given_state.location.heading_angle}"
            )
            logger.debug(
                f"{state['vehicle_speed'].values[0]} == {given_state.location.speed}"
            )
            logger.debug(
                f"{state['vehicle_x'].values[0]} == {given_state.location.position.x}"
            )
            logger.debug(
                f"{state['vehicle_y'].values[0]} == {given_state.location.position.y}"
            )

            self.assertEqual(
                state["vehicle_acceleration"].values[0],
                given_state.location.acceleration,
            )
            self.assertEqual(
                state["vehicle_angle"].values[0], given_state.location.heading_angle
            )
            self.assertEqual(
                state["vehicle_speed"].values[0], given_state.location.speed
            )
            self.assertEqual(
                state["vehicle_x"].values[0], given_state.location.position.x
            )
            self.assertEqual(
                state["vehicle_y"].values[0], given_state.location.position.y
            )

    def test_add_vehicle_state(self):
        trip = Trip.objects.first()
        dataset_trip_id = self.vehicles_states["vehicle_id"].unique()[0]
        states = self.vehicles_states[
            self.vehicles_states["vehicle_id"] == dataset_trip_id
        ]

        call_command(
            "addvehiclestates", trip_id=trip.id, dataset_trip_id=dataset_trip_id
        )

        stored_vehicle_states = trip.vehiclestate_set.all()

        self.compare_results(states, stored_vehicle_states)

    def test_cannot_add_states_to_closed_trip(self):
        call_command("closetrip", self.user.id)
        trip = Trip.objects.filter(user_id=self.user).first()

        with self.assertRaises(ValidationError):
            call_command("addvehiclestates", trip_id=trip.id, dataset_trip_id=0)

    def test_cannot_add_wrong_dataset_vehicle_id(self):
        trip = Trip.objects.filter(user_id=self.user).first()
        dataset_trip_id = max(self.vehicles_states["vehicle_id"].unique()) + 10

        with self.assertRaises(ValueError):
            call_command(
                "addvehiclestates", trip_id=trip.id, dataset_trip_id=dataset_trip_id
            )

    def test_cannot_add_wrong_trip_id(self):
        trip_id = 10 + (
            max(
                list(
                    Trip.objects.filter(user_id=self.user).values_list("id", flat=True)
                )
            )
        )

        dataset_trip_id = self.vehicles_states["vehicle_id"].unique()[0]

        with self.assertRaises(ValidationError):
            call_command(
                "addvehiclestates", trip_id=trip_id, dataset_trip_id=dataset_trip_id
            )
