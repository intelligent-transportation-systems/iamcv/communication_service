import logging
from typing import Type, Union

from django.core.management import call_command
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from api.models import Action, Property
from user.models import CustomUser
from vehicle.models import VehicleBody, VehicleManufacture
from vehicle_state.models import VehicleState

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

one_field_models = Union[
    Type[Action], Type[Property], Type[VehicleManufacture], Type[VehicleBody]
]


class VehicleStateViewTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.username = "test_user"
        cls.username2 = "test_user2"
        cls.password = "test_password"

        cls.user = CustomUser.objects.create_user(
            username=cls.username, password=cls.password
        )
        cls.user2 = CustomUser.objects.create_user(
            username=cls.username2, password=cls.password
        )

        call_command("addactions", ["test_action1", "test_action2"])
        call_command("addproperties", ["test_prop1", "test_prop2"])
        call_command("addvehiclemodels", 2)
        call_command("createvehicle", user_id=cls.user.id)
        call_command("createvehicle", user_id=cls.user2.id)
        call_command("createtrip", user_id=cls.user.id)
        call_command("createtrip", user_id=cls.user2.id)

        cls.trip = CustomUser.objects.get(id=cls.user.id).trip_set.get(end_at=None)
        cls.trip2 = CustomUser.objects.get(id=cls.user2.id).trip_set.get(end_at=None)

    def setUp(self):
        self.assertTrue(
            self.client.login(username=self.username, password=self.password)
        )

    def create_engine_state(self, **kwargs) -> dict:
        return {
            "rpm": kwargs.get("rpm", 2800),
            "power_consumption": kwargs.get("power_consumption", 456),
            "battery_charge": kwargs.get("battery_charge", 45),
        }

    def create_control_state(self, **kwargs) -> dict:
        return {
            "steering_wheel_angle": kwargs.get("steering_wheel_angle", 50),
            "gas_pedal_value": kwargs.get("gas_pedal_value", 20),
            "stop_pedal_value": kwargs.get("stop_pedal_value", 0),
        }

    def create_driver_state(self, **kwargs) -> dict:
        return {
            "concentrated_on_the_road": kwargs.get("concentrated_on_the_road", False),
        }

    def create_vehicle_state(self, **kwargs) -> dict:
        return {
            "time_stamp": kwargs["time_stamp"],
            "location": {
                "heading_angle": kwargs.get("heading_angle", 40.5),
                "acceleration": kwargs.get("acceleration", 2.5),
                "speed": kwargs.get("speed", 60.5),
                "position": kwargs["position"],
                "accuracy": kwargs.get("accuracy", 10),
                "road_name": kwargs.get("road_name", "test_road_name"),
            },
            "engine": kwargs.get("engine", self.create_engine_state()),
            "control": kwargs.get("control", self.create_control_state()),
            "driver": kwargs.get("driver", self.create_driver_state()),
        }

    def test_add_full_state(self):
        logger.debug(f"user: {self.user.username}")
        vehicle_state: dict = self.create_vehicle_state(
            time_stamp=timezone.now(), position="POINT(45.45 55.55)"
        )
        logger.debug(f"sended_data:{vehicle_state}")
        response = self.client.post(
            reverse("create-vehicle-state"),
            data=vehicle_state,
            content_type="application/json",
        )
        logger.debug(f"received_data:{response.json()}")

        self.assertEqual(response.status_code, 201)
        response_data: dict = response.json()

        vehicle_state: VehicleState = VehicleState.objects.get(id=response_data["id"])
        self.assertEqual(vehicle_state.location.id, response_data["location"]["id"])
        self.assertEqual(vehicle_state.control.id, response_data["control"]["id"])
        self.assertEqual(vehicle_state.engine.id, response_data["engine"]["id"])

    def test_add_part_state(self):
        logger.debug(f"user: {self.user.username}")

        vehicle_state: dict = self.create_vehicle_state(
            time_stamp=timezone.now(),
            position="POINT(45.45 55.55)",
            engine=None,
            control=None,
        )

        logger.debug(f"sended_data:{vehicle_state}")

        response = self.client.post(
            reverse("create-vehicle-state"),
            data=vehicle_state,
            content_type="application/json",
        )

        logger.debug(f"received_data:{response.json()}")

        self.assertEqual(response.status_code, 201)
        response_data: dict = response.json()

        vehicle_state: VehicleState = VehicleState.objects.get(id=response_data["id"])
        self.assertEqual(vehicle_state.location.id, response_data["location"]["id"])
        self.assertEqual(vehicle_state.control, response_data["control"])
        self.assertEqual(vehicle_state.engine, response_data["engine"])

    def test_cannot_add_states_in_closed_trip(self):
        logger.debug(f"user: {self.user.username}")
        self.trip.end_at = timezone.now()
        self.trip.save()

        vehicle_state: dict = self.create_vehicle_state(
            time_stamp=timezone.now(), position="POINT(45.45 55.55)"
        )
        logger.debug(f"sended_data:{vehicle_state}")
        response = self.client.post(
            reverse("create-vehicle-state"),
            data=vehicle_state,
            content_type="application/json",
        )
        logger.debug(f"received_data:{response.json()}")

        self.assertEqual(response.status_code, 400)

    def test_cannot_create_wrong_state(self):
        logger.debug(f"user: {self.user.username}")

        vehicle_state: dict = self.create_vehicle_state(
            time_stamp=timezone.now(),
            position="(45.45 55.55)",
            engine=None,
            control=None,
        )

        logger.debug(f"sended_data:{vehicle_state}")

        response = self.client.post(
            reverse("create-vehicle-state"),
            data=vehicle_state,
            content_type="application/json",
        )

        logger.debug(f"received_data:{response.json()}")

        self.assertEqual(response.status_code, 400)


class ListVehicleStateTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        call_command("addactions", ["test_action1", "test_action2"])
        call_command(
            "addproperties", ["average_acceleration", "average_speed", "drowsiness"]
        )
        call_command("filldb", trip_number=2)

    def test_can_get_vehicle_state(self):
        self.assertTrue(self.client.login(username="user 0", password="password"))
        states_resp = self.client.get(reverse("list-vehicle-state"))
        self.assertEqual(states_resp.status_code, 200)

        states = states_resp.json()
        trips = CustomUser.objects.get(username="user 0").trip_set.values_list(
            "id", flat=True
        )

        expected_states = VehicleState.objects.filter(trip_id__in=trips)
        for e_state, state in zip(expected_states, states):
            self.assertEqual(e_state.id, state["id"])
            self.assertTrue("location" in state)
            self.assertTrue("driver" in state)
            self.assertTrue("engine" in state)
            self.assertTrue("control" in state)

    def test_can_filter_by_trip(self):
        trip_id = CustomUser.objects.get(username="user 0").trip_set.first().id
        self.assertTrue(self.client.login(username="user 0", password="password"))
        states_resp = self.client.get(
            reverse("list-vehicle-state"), {"trip_id": trip_id}
        )
        self.assertEqual(states_resp.status_code, 200)

        states = states_resp.json()

        expected_states = VehicleState.objects.filter(trip_id=trip_id)
        for e_state, state in zip(expected_states, states):
            self.assertEqual(e_state.id, state["id"])
            self.assertTrue("location" in state)
            self.assertTrue("driver" in state)
            self.assertTrue("engine" in state)
            self.assertTrue("control" in state)

    def test_cannot_request_foreign_states(self):
        trip_id = CustomUser.objects.get(username="user 1").trip_set.first().id
        self.assertTrue(self.client.login(username="user 0", password="password"))
        states_resp = self.client.get(
            reverse("list-vehicle-state"), {"trip_id": trip_id}
        )
        self.assertEqual(states_resp.status_code, 200)

        states = states_resp.json()

        self.assertTrue(not states)
