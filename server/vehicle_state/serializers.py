from rest_framework import serializers
from rest_framework_gis.serializers import GeoFeatureModelSerializer

from trip.serializers import NeighborTripSerializer

from .models import ControlState, DriverState, EngineState, LocationState, VehicleState


class ControlStateSerializer(serializers.ModelSerializer):
    class Meta:
        model = ControlState
        fields = "__all__"


class LocationStateSerializer(GeoFeatureModelSerializer):
    class Meta:
        model = LocationState
        geo_field = "position"
        fields = "__all__"


class EngineStateSerializer(serializers.ModelSerializer):
    class Meta:
        model = EngineState
        fields = "__all__"


class DriverStateSerializer(serializers.ModelSerializer):
    class Meta:
        model = DriverState
        fields = "__all__"


class VehicleStateSerializer(serializers.ModelSerializer):
    location = LocationStateSerializer()
    control = ControlStateSerializer(allow_null=True)
    engine = EngineStateSerializer(allow_null=True)
    driver = DriverStateSerializer(allow_null=True)

    def _create_object(self, validated_data, model):
        if validated_data is None:
            return None

        return model.objects.create(**validated_data)

    def validate_trip_id(self, trip):
        if self.context.get("request", False):
            if trip not in self.context["request"].user.trip_set.all():
                raise serializers.ValidationError(
                    f"User does not have given trip id = {trip}"
                )

        if trip.end_at is not None:
            raise serializers.ValidationError(
                f"the trip {trip.id} is closed, you cannot added states into it"
            )
        return trip

    def create(self, validated_data):
        control_obj = self._create_object(validated_data.get("control"), ControlState)
        location_obj = self._create_object(
            validated_data.get("location"), LocationState
        )
        engine_obj = self._create_object(validated_data.get("engine"), EngineState)
        driver_obj = self._create_object(validated_data.get("driver"), DriverState)

        validated_data = {
            **validated_data,
            # it will overwrite given keys on given data
            **{
                "location": location_obj,
                "engine": engine_obj,
                "control": control_obj,
                "driver": driver_obj,
            },
        }
        vehicle_state = VehicleState.objects.create(**validated_data)

        return vehicle_state

    class Meta:
        model = VehicleState
        fields = [
            "id",
            "time_stamp",
            "trip_id",
            "location",
            "engine",
            "control",
            "driver",
        ]


class NeighborsLocationSerializer(GeoFeatureModelSerializer):
    class Meta:
        model = LocationState
        geo_field = "position"
        fields = [
            "heading_angle",
            "speed",
            "position",
            "accuracy",
            "road_name",
        ]


class NeighborsStateSerializer(serializers.ModelSerializer):
    trip = NeighborTripSerializer(source="trip_id")
    location = NeighborsLocationSerializer()

    class Meta:
        model = VehicleState
        fields = ["trip", "location"]
