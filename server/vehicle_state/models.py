from django.contrib.gis.db import models

from trip.models import Trip


class DriverState(models.Model):
    concentrated_on_the_road = models.BooleanField()


class LocationState(models.Model):
    heading_angle = models.FloatField()
    acceleration = models.FloatField()  # m/s
    speed = models.FloatField()  # m/s

    position = models.PointField(db_index=True)

    accuracy = models.FloatField()
    road_name = models.CharField(max_length=100, null=True, blank=True)

    class Meta:
        constraints = [
            models.CheckConstraint(
                name="check_range_heading_angle",
                check=models.Q(heading_angle__range=(0, 360)),
            ),
            models.CheckConstraint(
                name="check_accuracy_range", check=models.Q(accuracy__range=(0, 100))
            ),
        ]


# ToDo: it needs to be clarified
class ControlState(models.Model):
    steering_wheel_angle = models.FloatField()
    gas_pedal_value = models.FloatField()
    stop_pedal_value = models.FloatField()

    class Meta:
        constraints = [
            models.CheckConstraint(
                name="check_steering_wheel_angle",
                check=models.Q(steering_wheel_angle__range=(-720, 720)),
            ),
            models.CheckConstraint(
                name="check_gas_pedal_value",
                check=models.Q(gas_pedal_value__range=(0, 100)),
            ),
            models.CheckConstraint(
                name="check_stop_pedal_value",
                check=models.Q(stop_pedal_value__range=(0, 100)),
            ),
        ]


# ToDo: it needs to be clarified
class EngineState(models.Model):
    rpm = models.FloatField()
    power_consumption = models.FloatField()
    battery_charge = models.FloatField()

    class Meta:
        constraints = [
            models.CheckConstraint(name="check_rpm_angle", check=models.Q(rpm__gte=0)),
            models.CheckConstraint(
                name="check_battery_charge_angle",
                check=models.Q(battery_charge__range=(0, 100)),
            ),
        ]


class VehicleState(models.Model):
    trip_id = models.ForeignKey(Trip, on_delete=models.CASCADE, db_index=True)

    time_stamp = models.DateTimeField(db_index=True)

    location = models.OneToOneField(
        LocationState, on_delete=models.CASCADE, db_index=True
    )
    control = models.OneToOneField(
        ControlState, null=True, blank=True, on_delete=models.CASCADE
    )
    engine = models.OneToOneField(
        EngineState, null=True, blank=True, on_delete=models.CASCADE
    )
    driver = models.OneToOneField(
        DriverState, null=True, blank=True, on_delete=models.CASCADE
    )

    def __str__(self):
        return f"trip {self.trip_id}: {self.time_stamp}"
