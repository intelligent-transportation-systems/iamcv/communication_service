from django.urls import path

from .views import CreateVehicleState, ListVehicleState

urlpatterns = [
    path("add", CreateVehicleState.as_view(), name="create-vehicle-state"),
    path("list", ListVehicleState.as_view(), name="list-vehicle-state"),
]
