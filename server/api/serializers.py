from rest_framework import serializers

from trip.models import Trip
from vehicle.models import Vehicle
from vehicle_state.serializers import NeighborsStateSerializer

from .business_logic.clustering import VehicleCluster
from .business_logic.properties.handlers import NeighborProperty, get_handler
from .models import Action, Property


class PropertySerializer(serializers.ModelSerializer):
    class Meta:
        model = Property
        fields = "__all__"


class ActionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Action
        fields = "__all__"


class VehiclePropertySerializer(serializers.Serializer):
    trip_pk = serializers.IntegerField(write_only=True)
    property_pk = serializers.IntegerField(write_only=True)

    property_name = serializers.SerializerMethodField()
    property_value = serializers.SerializerMethodField()

    def create(self):
        pass

    def update(self):
        pass

    def validate_trip_pk(self, trip_pk: int):

        requested_trip = Trip.objects.get(id=trip_pk)

        if "request" in self.context:
            cluster = VehicleCluster(self.context["request"].user)

            neighbors_trips = cluster.get_cached_neighbors_states().values_list(
                "trip_id", flat=True
            )

            if requested_trip.id not in neighbors_trips:
                raise serializers.ValidationError(
                    f"requested trip {trip_pk} is not in available list"
                )

        return trip_pk

    def validate(self, data: dict):
        requested_vehicle = Trip.objects.get(id=data["trip_pk"]).vehicle
        if not isinstance(requested_vehicle, Vehicle):
            raise serializers.ValidationError(
                f"trip {data['trip_pk']} does not have a vehicle"
            )

        if data["property_pk"] not in requested_vehicle.allowed_properties.values_list(
            "id", flat=True
        ):
            raise serializers.ValidationError(
                f"vehicle {data['trip_pk']} does not allowed request property {data['property_pk']}"
            )

        return super(VehiclePropertySerializer, self).validate(data)

    def get_property_name(self, obj: dict):
        vehicle_property = Property.objects.get(id=obj["property_pk"])
        serializer = PropertySerializer(vehicle_property)
        return serializer.data

    def get_property_value(self, obj: dict):
        handler = get_handler()
        return handler.run_handler(NeighborProperty(**obj))


class NeighborsSerializer(NeighborsStateSerializer):
    pass
