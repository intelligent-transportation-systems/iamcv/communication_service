from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.response import Response

from .business_logic.clustering import VehicleCluster, VehicleClusterInt
from .models import Action, Property
from .serializers import (
    ActionSerializer,
    NeighborProperty,
    NeighborsSerializer,
    PropertySerializer,
    VehiclePropertySerializer,
)


class PropertiesListView(ListAPIView):
    queryset = Property.objects.all()
    serializer_class = PropertySerializer


class ActionListView(ListAPIView):
    queryset = Action.objects.all()
    serializer_class = ActionSerializer


class NeighborsListView(ListAPIView):
    serializer_class = NeighborsSerializer

    def get_queryset(self):
        cluster: VehicleClusterInt = VehicleCluster(self.request.user)
        cluster.is_valid(raise_exception=True)
        return cluster.get_neighbors_states(self.kwargs["within_distance_m"])


class PropertyView(RetrieveAPIView):
    serializer_class = VehiclePropertySerializer

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(data=instance, partial=True)

        if not serializer.is_valid(raise_exception=True):
            return Response(serializer.errors)

        return Response(serializer.data)

    def get_object(self):
        return NeighborProperty(**self.kwargs).dict()
