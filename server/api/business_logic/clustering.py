from abc import ABC, abstractmethod
from datetime import timedelta

from django.contrib.gis.measure import Distance
from django.core.cache import cache
from django.db.models import QuerySet
from rest_framework.serializers import ValidationError
from user.models import CustomUser
from vehicle_state.models import LocationState, VehicleState


class VehicleClusterInt(ABC):
    def __init__(self, user_model: CustomUser, time_shift=5):
        self.user_model = user_model
        self.time_shift = timedelta(seconds=time_shift)

    def __str__(self) -> str:
        return f"cluster.{self.user_model.id}"

    @abstractmethod
    def get_neighbors_states(self, within_distance_m: int) -> QuerySet[VehicleState]:
        """method return list of vehicles near the user"""

    @abstractmethod
    def is_valid(self, raise_exception=False) -> bool:
        """method validates the possibility to return neighbors"""


class VehicleCluster(VehicleClusterInt):
    def is_valid(self, raise_exception=False) -> bool:
        if not self.user_model.trip_set.filter(end_at=None).exists():
            if raise_exception:
                raise ValidationError(
                    {"trip": "you need to have an active trip to request neighbors"}
                )

            return False

        if (
            not self.user_model.trip_set.get(end_at=None)
            .vehiclestate_set.all()
            .exists()
        ):
            if raise_exception:
                raise ValidationError(
                    {
                        "vehiclestate": "you need to have at least one state to request the neighbors"
                    }
                )

            return False

        return True

    def _get_last_user_location_state(self) -> LocationState:
        user_location = (
            self.user_model.trip_set.get(end_at=None)
            .vehiclestate_set.latest("time_stamp")
            .location
        )
        return user_location

    def _get_last_slice_of_states(self, user_location: LocationState):
        users_timestamp = user_location.vehiclestate.time_stamp
        time_range = users_timestamp - self.time_shift

        last_states = (
            VehicleState.objects.exclude(trip_id=user_location.vehiclestate.trip_id.id)
            .filter(
                trip_id__end_at=None,
                time_stamp__gte=time_range,
            )
            .order_by("trip_id", "-time_stamp")
            .distinct("trip_id")
        )
        return last_states

    def _compute_neighbors_locations(
        self,
        state_slice: QuerySet[VehicleState],
        user_location: LocationState,
        within_m: int,
    ) -> QuerySet[VehicleState]:

        neighbor_states = VehicleState.objects.filter(
            id__in=state_slice,
            location__position__distance_lt=(
                user_location.position,
                Distance(m=within_m),
            ),
        )

        return neighbor_states

    def get_cached_neighbors_states(self) -> QuerySet[VehicleState]:
        return cache.get(f"{self}.last_neighbors")

    def get_neighbors_states(self, within_m: int) -> QuerySet[VehicleState]:
        last_user_location = self._get_last_user_location_state()

        if last_user_location is None:
            return []

        last_states_slice = self._get_last_slice_of_states(last_user_location)
        neighbor_states = self._compute_neighbors_locations(
            last_states_slice, last_user_location, within_m
        )

        cache.set(f"{self}.last_neighbors", neighbor_states)

        return neighbor_states
