import logging
from abc import ABC, abstractmethod
from datetime import timedelta
from typing import Any

from django.db.models import Avg
from pydantic import BaseModel

from api.models import Property
from trip.models import Trip
from vehicle_state.models import DriverState, LocationState, VehicleState

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


# properties should have the same name as defined in URLs
class NeighborProperty(BaseModel):
    trip_pk: int
    property_pk: int


class PropertyHandlerInt(ABC):
    @abstractmethod
    def set_next(self, handler):
        ...

    @abstractmethod
    def run_handler(self, vehicle_property: NeighborProperty) -> Any:
        ...


class DefaultPropertyHandler(PropertyHandlerInt):
    handled_property: str = ""

    def __init__(self, next_handler=None):
        self.handled_property_id = Property.objects.get(name=self.handled_property).id
        self._next_handler: PropertyHandlerInt = next_handler

    def __str__(self):
        return type(self).__name__

    def set_next(self, handler: PropertyHandlerInt) -> PropertyHandlerInt:
        self._next_handler = handler
        return handler

    def handle(self, trip: Trip) -> Any:
        pass

    def run_handler(self, neighbor_property: NeighborProperty) -> Any:
        logger.debug(f"enter to {self} class handler method")
        logger.debug(
            f"given property: {neighbor_property.property_pk}, handled property {self.handled_property}"
        )

        if not isinstance(neighbor_property, NeighborProperty):
            raise TypeError("vehicle_property has to be VehicleProperty's instance")

        if neighbor_property.property_pk == self.handled_property_id:
            logger.debug(f" {self}: enter to the handle method")
            return self.handle(Trip.objects.get(id=neighbor_property.trip_pk))

        elif self._next_handler:
            logger.debug(f"{self}: enter to the run next handler method")
            return self._next_handler.run_handler(neighbor_property)

        else:
            logger.debug(f"{self}: enter to the default method")
            return f"There is not handler for given property {neighbor_property.property_pk}"


class AverageSpeedHandler(DefaultPropertyHandler):
    handled_property = "average_speed"

    def handle(self, trip: Trip) -> Any:

        return LocationState.objects.filter(vehiclestate__trip_id=trip.id).aggregate(
            Avg("speed")
        )


class AverageAccHandler(DefaultPropertyHandler):
    handled_property = "average_acceleration"

    def handle(self, trip: Trip) -> Any:

        return LocationState.objects.filter(vehiclestate__trip_id=trip.id).aggregate(
            Avg("acceleration")
        )


class DriverDrowsinessHandler(DefaultPropertyHandler):
    handled_property = "drowsiness"

    def handle(self, trip: Trip) -> Any:
        latest_vehicle_state = VehicleState.objects.filter(trip_id=trip.id).latest(
            "time_stamp"
        )
        time_range = timedelta(hours=1)
        drowsiness_states = DriverState.objects.filter(
            vehiclestate__time_stamp__gte=latest_vehicle_state.time_stamp - time_range,
            vehiclestate__trip_id=trip.id,
        )
        driver_states_number = drowsiness_states.count()

        if driver_states_number <= 100:
            return "not enough data"

        number_drowsy_states = drowsiness_states.filter(
            concentrated_on_the_road=False
        ).count()

        drowsiness = round(number_drowsy_states / driver_states_number, 2)
        return {"drowsiness": drowsiness}


# method returns a built chain
def get_handler() -> PropertyHandlerInt:
    speed_handler = AverageSpeedHandler()
    acc_handler = AverageAccHandler()
    acc_handler.set_next(DriverDrowsinessHandler())
    speed_handler.set_next(acc_handler)

    return speed_handler
