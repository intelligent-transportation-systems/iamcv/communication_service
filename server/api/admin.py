from django.contrib import admin

from .models import Action, Property

admin.site.register(Action)
admin.site.register(Property)
