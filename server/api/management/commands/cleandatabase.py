from django.core.management.base import BaseCommand

from api.models import Action, Property
from trip.models import Trip
from user.models import CustomUser
from vehicle.models import (
    Vehicle,
    VehicleBody,
    VehicleManufacture,
    VehicleModel,
    VehicleModelYear,
)
from vehicle_state.models import ControlState, EngineState, LocationState, VehicleState


class Command(BaseCommand):
    help = "remove data from all of the tables. It's usefull for tests"

    def handle(self, *args, **options):
        CustomUser.objects.all().delete()
        Action.objects.all().delete()
        Property.objects.all().delete()
        Vehicle.objects.all().delete()
        VehicleModel.objects.all().delete()
        VehicleBody.objects.all().delete()
        VehicleManufacture.objects.all().delete()
        VehicleModelYear.objects.all().delete()
        LocationState.objects.all().delete()
        VehicleState.objects.all().delete()
        EngineState.objects.all().delete()
        ControlState.objects.all().delete()
        Trip.objects.all().delete()
