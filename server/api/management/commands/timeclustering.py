import logging
import timeit

from django.core.management import BaseCommand

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


class Command(BaseCommand):
    help = "measure time that is needed to return neighbors"

    def add_arguments(self, parser):
        parser.add_argument(
            "--number", type=int, help="timeit number property", default=100
        )
        parser.add_argument(
            "--repeat", type=int, help="timeit repeat property", default=10
        )

    def handle(self, *args, **options):
        setup = (
            "from api.business_logic.clustering import VehicleCluster\n"
            "from django.contrib.auth.models import User\n"
            "cluster = VehicleCluster(User.objects.first())\n"
        )

        run = "cluster.get_neighbors(within_distance_m=100)"

        measurements = timeit.repeat(
            setup=setup, stmt=run, number=options["number"], repeat=options["repeat"]
        )

        print(
            f"min = {min(measurements)},\n"
            f"max = {max(measurements)},\n"
            f"average = {sum(measurements)/len(measurements)}"
        )
