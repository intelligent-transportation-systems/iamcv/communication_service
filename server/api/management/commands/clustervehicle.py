import random
from typing import Type, Union

import gmplot
import numpy as np
from django.core.management.base import BaseCommand
from geopy.distance import geodesic
from sklearn.cluster import DBSCAN, AgglomerativeClustering
from sklearn.metrics import pairwise_distances
from vehicle_state.models import LocationState


class Command(BaseCommand):
    help = "clustering positions"

    def add_arguments(self, parser):
        parser.add_argument(
            "--distance_threshold",
            type=float,
            default=200,
            help="distance threshold in meters for clustering",
        )
        parser.add_argument(
            "--cluster_type",
            type=str,
            choices=["DBSCAN", "Agglomerative"],
            default="DBSCAN",
            help="1 DBSCAN, 2",
        )
        parser.add_argument(
            "--build_map",
            type=bool,
            default=False,
            help="create map.html file",
        )

    def _metric(self, x: np.ndarray, y: np.ndarray) -> float:
        return float(geodesic(x, y).m)

    def _affinity(self, x: np.ndarray):
        return pairwise_distances(x, metric=self._metric)

    def get_agglomerative_instance(
        self, distance_threshold: float
    ) -> AgglomerativeClustering:

        return AgglomerativeClustering(
            distance_threshold=distance_threshold,
            n_clusters=None,
            affinity=self._affinity,
            linkage="single",
        )

    def get_dbscan_instance(self, distance_threshold: float) -> DBSCAN:
        return DBSCAN(eps=distance_threshold, metric=self._metric)

    def get_cluster_model(
        self, cluster_type: str, distance_threshold: float
    ) -> Union[Type[AgglomerativeClustering], Type[DBSCAN]]:

        cluster = {
            "DBSCAN": self.get_dbscan_instance,
            "Agglomerative": self.get_agglomerative_instance,
        }
        return cluster[cluster_type](distance_threshold)

    def build_map(self, positions: np.ndarray, labels: np.ndarray):
        gmap = gmplot.GoogleMapPlotter(
            (max(positions[:, 0]) + min(positions[:, 0])) / 2,
            (max(positions[:, 1]) + min(positions[:, 1])) / 2,
            15,
        )

        unique_labels = np.unique(labels)

        for label in range(len(unique_labels)):
            hexadecimal = [
                "#" + "".join([random.choice("ABCDEF0123456789") for i in range(6)])
            ]

            gmap.scatter(
                positions[labels == label, 0],
                positions[labels == label, 1],
                color=hexadecimal[0],
                size=40,
                marker=False,
            )

        gmap.draw("map.html")

    def create_cluster_dict(self, cluster_list: np.ndarray):
        pass

    def handle(self, *args, **options):
        locations = (
            LocationState.objects.filter(vehiclestate__trip_id__end_at=None)
            .order_by("vehiclestate__trip_id", "-vehiclestate__time_stamp")
            .distinct("vehiclestate__trip_id")
        )

        positions = np.array([(loc.position.y, loc.position.x) for loc in locations])

        cluster_model = self.get_cluster_model(
            cluster_type=options["cluster_type"],
            distance_threshold=options["distance_threshold"],
        )

        cluster = cluster_model.fit(positions)
        labels = cluster.labels_
        unique_labels = np.unique(labels)

        for label in range(len(unique_labels)):
            pass

        if options["build_map"]:
            self.build_map(positions=positions, labels=labels)
