from random import choice

import numpy as np
import pandas as pd
from django.core.exceptions import ValidationError
from django.core.management import call_command
from django.core.management.base import BaseCommand
from django.db import transaction
from django.db.models import QuerySet

from api.models import Action, Property
from RUCS.vehicle_state_data.dataset import get_dataset
from user.models import CustomUser
from vehicle.models import VehicleModel


class Command(BaseCommand):
    help = "add users, trip and states from the dataset to the db"

    def add_arguments(self, parser):
        parser.add_argument(
            "--trip_number", type=int, default=float("inf"), help="properties' names"
        )

    def _get_dataset(self) -> pd.DataFrame:
        return get_dataset()

    @transaction.atomic
    def handle(self, *args, **options):
        props = Property.objects.values_list("id", flat=True)
        actions = Action.objects.values_list("id", flat=True)

        if len(actions) == 0 or len(props) == 0:
            raise ValidationError("actions and properties cannot be empty")

        dataset: pd.DataFrame = self._get_dataset()
        dataset_vehicle_list: np.ndarray = dataset["vehicle_id"].unique()

        # limit number of vehicles
        dataset_vehicle_list = dataset_vehicle_list[: options["trip_number"]]
        call_command("addvehiclemodels", len(dataset_vehicle_list))

        models = VehicleModel.objects.all()

        # for each vehicle_id create user, vehicle, open trip and store vehicle states
        for vehicle_id in dataset_vehicle_list:
            user: QuerySet = CustomUser.objects.create_user(
                username=f"user {vehicle_id}",
                email="user_vehicle_id@gmail.com",
                password="password",
            )

            model = choice(models)
            call_command(
                "createvehicle",
                user_id=user.id,
                actions=actions,
                properties=props,
                model=model.id,
            )

            vehicle = user.vehicle_set.first()
            call_command("createtrip", user_id=user.id, vehicle_id=vehicle.id)
            trip = user.trip_set.filter(end_at=None).first()
            call_command(
                "addvehiclestates", trip_id=trip.id, dataset_trip_id=vehicle_id
            )
