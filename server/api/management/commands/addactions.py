import logging

from django.core.management.base import BaseCommand
from django.db import transaction

from api.models import Action

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


class Command(BaseCommand):
    help = "add actions to the db"

    def add_arguments(self, parser):
        parser.add_argument("actions", nargs="+", type=str, help="actions' names")

    def handle(self, *args, **options):
        with transaction.atomic():
            for action in options["actions"]:
                Action.objects.create(name=action)

                logger.debug(f"{action} action added to the Action table")
