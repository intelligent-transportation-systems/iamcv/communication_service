import logging

from django.core.management import BaseCommand
from django.db import transaction

from api.models import Property

logger = logging.getLogger(__name__)

logging.basicConfig(level=logging.DEBUG)


class Command(BaseCommand):
    help = "Add properties to the db"

    def add_arguments(self, parser):
        parser.add_argument("properties", nargs="+", type=str, help="properties' names")

    def handle(self, *args, **options):
        with transaction.atomic():
            for prop in options["properties"]:
                Property.objects.create(name=prop)

                logger.debug(f"{prop} property added tot the Property table")
