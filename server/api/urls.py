from django.urls import include, path

from .views import ActionListView, NeighborsListView, PropertiesListView, PropertyView

urlpatterns = [
    path("properties-list", PropertiesListView.as_view(), name="properties-list"),
    path("actions-list", ActionListView.as_view(), name="actions-list"),
    path(
        "property/<int:property_pk>/from-trip/<int:trip_pk>",
        PropertyView.as_view(),
        name="request-property",
    ),
    path(
        "neighbors/<int:within_distance_m>",
        NeighborsListView.as_view(),
        name="get-neighbors",
    ),
    path("vehicle/", include("vehicle.urls")),
    path("vehicle-state/", include("vehicle_state.urls")),
    path("trip/", include("trip.urls")),
    path("user/", include("user.urls")),
]
