from django.db.utils import IntegrityError
from django.test import TransactionTestCase

from api.models import Action, Property


class ActionModelTest(TransactionTestCase):
    def test_unique_name(self):
        with self.assertRaises(IntegrityError):
            Action.objects.create(name="test")
            Action.objects.create(name="test")


class PropertyModelTest(TransactionTestCase):
    def test_unique_name(self):
        with self.assertRaises(IntegrityError):
            Property.objects.create(name="test")
            Property.objects.create(name="test")
