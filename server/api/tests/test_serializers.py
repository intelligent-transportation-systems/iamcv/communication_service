from django.core.management import call_command
from django.test import TestCase
from rest_framework.serializers import ValidationError

from api.business_logic.properties.handlers import NeighborProperty, get_handler
from api.models import Property
from api.serializers import VehiclePropertySerializer
from trip.models import Trip
from user.models import CustomUser
from vehicle.models import Vehicle


class TestVehiclePropertySerializer(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = CustomUser.objects.create_user(
            username="testuser", password="testpassword"
        )
        cls.user2 = CustomUser.objects.create_user(
            username="testuser2", password="testpassword"
        )
        call_command("addactions", ["testaction1", "testaction2"])
        call_command(
            "addproperties", ["average_speed", "average_acceleration", "drowsiness"]
        )
        call_command("addvehiclemodels", 2)

        call_command("createvehicle", user_id=cls.user.id)
        call_command("createtrip", user_id=cls.user.id)
        trip = cls.user.trip_set.get(end_at=None)
        call_command("addvehiclestates", trip_id=trip.id, dataset_trip_id=1)

        call_command(
            "createvehicle",
            user_id=cls.user2.id,
            properties=Property.objects.first().id,
        )
        call_command("createtrip", user_id=cls.user2.id)
        trip = cls.user2.trip_set.get(end_at=None)
        call_command("addvehiclestates", trip_id=trip.id, dataset_trip_id=2)

    def test_serializer(self):
        trip = Trip.objects.first()
        prop = trip.vehicle.allowed_properties.get(name="average_speed")
        requested_data = {"trip_pk": trip.id, "property_pk": prop.id}
        property_serializer = VehiclePropertySerializer(
            data=requested_data,
            partial=True,
        )
        self.assertTrue(property_serializer.is_valid())
        data = property_serializer.data

        self.assertEqual(data["property_name"]["id"], prop.id)
        self.assertEqual(data["property_name"]["name"], prop.name)

        handler = get_handler()

        expected_average_speed = handler.run_handler(NeighborProperty(**requested_data))

        self.assertEqual(data["property_value"], expected_average_speed)

    def test_cannot_request_wrong_property(self):
        vehicle = Vehicle.objects.get(user_id=self.user2)
        prop = Property.objects.get(name="average_acceleration")
        requested_data = {"trip_pk": vehicle.id, "property_pk": prop.id}

        property_serializer = VehiclePropertySerializer(
            data=requested_data,
            partial=True,
        )

        with self.assertRaises(ValidationError):
            property_serializer.is_valid(raise_exception=True)

        self.assertTrue(
            "not allowed" in property_serializer.errors["non_field_errors"][0]
        )
