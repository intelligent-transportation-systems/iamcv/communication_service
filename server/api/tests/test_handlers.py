import logging
import random

from django.core.management import call_command
from django.db.models import QuerySet
from django.test import TestCase

from api.business_logic.properties.handlers import (
    AverageAccHandler,
    AverageSpeedHandler,
    DriverDrowsinessHandler,
    NeighborProperty,
    get_handler,
)
from api.models import Property
from trip.models import Trip
from user.models import CustomUser
from vehicle_state.models import DriverState, LocationState, VehicleState

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


class TestHandlers(TestCase):
    @staticmethod
    def fill_database(user_id: int):
        call_command("addactions", ["testaction1", "testaction2"])
        call_command(
            "addproperties", ["average_speed", "average_acceleration", "drowsiness"]
        )
        call_command("addvehiclemodels", 2)

        call_command("createvehicle", user_id=user_id)
        call_command("createtrip", user_id=user_id)
        trip = CustomUser.objects.get(id=user_id).trip_set.get(end_at=None)
        call_command("addvehiclestates", trip_id=trip.id, dataset_trip_id=1)

    @classmethod
    def setUpTestData(cls):
        cls.user = CustomUser.objects.create_user(
            username="testuser", password="testpassword"
        )
        cls.fill_database(cls.user.id)

    def test_average_speed(self):
        trip = Trip.objects.get(end_at=None)
        prop = Property.objects.get(name="average_speed")

        speed_points = LocationState.objects.filter(
            vehiclestate__trip_id=trip.id
        ).values_list("speed", flat=True)
        expected_average_speed = sum(speed_points) / len(speed_points)

        vehicle_property = NeighborProperty(property_pk=prop.id, trip_pk=trip.id)
        handler = AverageSpeedHandler()
        average_speed = handler.run_handler(vehicle_property)

        logger.debug(f"given result: {average_speed}")

        self.assertEqual(average_speed["speed__avg"], expected_average_speed)

    def test_average_acc(self):
        trip = Trip.objects.get(end_at=None)
        prop = Property.objects.get(name="average_acceleration")

        acc_points = LocationState.objects.filter(
            vehiclestate__trip_id=trip.id
        ).values_list("acceleration", flat=True)
        expected_average_acc = sum(acc_points) / len(acc_points)

        vehicle_property = NeighborProperty(property_pk=prop.id, trip_pk=trip.id)
        handler = AverageAccHandler()
        average_acceleration = handler.run_handler(vehicle_property)

        logger.debug(f"given result: {average_acceleration}")

        self.assertEqual(
            average_acceleration["acceleration__avg"], expected_average_acc
        )

    def test_drowsiness(self):
        trip = Trip.objects.get(end_at=None)
        vehicle_states: QuerySet[VehicleState] = trip.vehiclestate_set.all()

        driver_drowsiness = []

        for state in vehicle_states:
            concentrated_state = random.choice([True, False])
            driver_drowsiness.append(concentrated_state)

            state.driver = DriverState.objects.create(
                concentrated_on_the_road=concentrated_state
            )
            state.save()

        prop = Property.objects.get(name="drowsiness")

        vehicle_property = NeighborProperty(property_pk=prop.id, trip_pk=trip.id)
        handler = DriverDrowsinessHandler()
        drowsiness = handler.run_handler(vehicle_property)

        drowsy_states = [state for state in driver_drowsiness if not state]

        expected_value = round(len(drowsy_states) / len(driver_drowsiness), 2)

        logger.debug(f"given result: {drowsiness}, expected result: {expected_value}")

        self.assertEqual(drowsiness["drowsiness"], expected_value)

    def test_can_find_correct_handler(self):
        trip = Trip.objects.get(end_at=None)
        vehicle_states: QuerySet[VehicleState] = trip.vehiclestate_set.all()

        driver_drowsiness = []

        for state in vehicle_states:
            concentrated_state = random.choice([True, False])
            driver_drowsiness.append(concentrated_state)

            state.driver = DriverState.objects.create(
                concentrated_on_the_road=concentrated_state
            )
            state.save()

        acc_prop = Property.objects.get(name="average_acceleration")
        speed_prop = Property.objects.get(name="average_speed")
        drowsiness_prop = Property.objects.get(name="drowsiness")

        acc_points = LocationState.objects.filter(
            vehiclestate__trip_id=trip.id
        ).values_list("acceleration", flat=True)

        expected_average_acc = sum(acc_points) / len(acc_points)

        speed_points = LocationState.objects.filter(
            vehiclestate__trip_id=trip.id
        ).values_list("speed", flat=True)
        expected_average_speed = sum(speed_points) / len(speed_points)

        vehicle_property = NeighborProperty(property_pk=acc_prop.id, trip_pk=trip.id)
        handler = get_handler()
        average_acceleration = handler.run_handler(vehicle_property)

        self.assertEqual(
            average_acceleration["acceleration__avg"], expected_average_acc
        )

        vehicle_property = NeighborProperty(property_pk=speed_prop.id, trip_pk=trip.id)
        handler = get_handler()
        average_speed = handler.run_handler(vehicle_property)

        self.assertEqual(average_speed["speed__avg"], expected_average_speed)

        vehicle_property = NeighborProperty(
            property_pk=drowsiness_prop.id, trip_pk=trip.id
        )
        handler = get_handler()
        drowsiness = handler.run_handler(vehicle_property)

        drowsy_states = [state for state in driver_drowsiness if not state]
        drowsiness_expected_value = round(
            len(drowsy_states) / len(driver_drowsiness), 2
        )

        self.assertEqual(drowsiness["drowsiness"], drowsiness_expected_value)
