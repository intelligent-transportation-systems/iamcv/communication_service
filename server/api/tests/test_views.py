import datetime
import logging
import random

from api.business_logic.clustering import VehicleCluster
from api.business_logic.properties.handlers import NeighborProperty, get_handler
from api.models import Action, Property
from django.core.management import call_command
from django.test import TestCase
from django.urls import reverse
from trip.models import Trip
from user.models import CustomUser
from vehicle_state.models import DriverState

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


class TestPropertyList(TestCase):
    @classmethod
    def setUpTestData(cls):
        call_command("addproperties", ["prop1", "prop2", "prop3"])
        cls.username = "user"
        cls.user_password = "password"
        cls.user = CustomUser.objects.create_user(
            username=cls.username, password=cls.user_password
        )

    def test_properties_list(self):
        self.client.login(username=self.username, password=self.user_password)

        response = self.client.get(reverse("properties-list"))
        expected_list_of_properties = list(Property.objects.all())

        logging.debug(f"response: {response.json()}")
        self.assertEqual(response.status_code, 200)

        given_list_of_properties = response.json()

        expected_list_of_properties.sort(key=lambda prop: prop.id)
        given_list_of_properties.sort(key=lambda prop: prop["id"])

        self.assertEqual(
            len(expected_list_of_properties), len(given_list_of_properties)
        )

        zipped_list = zip(expected_list_of_properties, given_list_of_properties)

        for expected_prop, given_prop in zipped_list:
            self.assertEquals(expected_prop.id, given_prop["id"])
            self.assertEquals(expected_prop.name, given_prop["name"])

    def test_cannot_request_without_login(self):
        response = self.client.get(reverse("properties-list"))
        self.assertEqual(response.status_code, 403)

        error = response.json()

        self.assertTrue("not provided" in error["detail"])


class TestActionsList(TestCase):
    @classmethod
    def setUpTestData(cls):
        call_command("addactions", ["action1", "action2", "action3"])
        cls.username = "user"
        cls.user_password = "password"
        cls.user = CustomUser.objects.create_user(
            username=cls.username, password=cls.user_password
        )

    def test_actions_list(self):
        self.client.login(username=self.username, password=self.user_password)

        response = self.client.get(reverse("actions-list"))
        expected_list_of_actions = list(Action.objects.all())

        logging.debug(f"response: {response.json()}")
        self.assertEqual(response.status_code, 200)

        given_list_of_actions = response.json()

        expected_list_of_actions.sort(key=lambda action: action.id)
        given_list_of_actions.sort(key=lambda action: action["id"])

        self.assertEqual(len(expected_list_of_actions), len(given_list_of_actions))

        zipped_list = zip(expected_list_of_actions, given_list_of_actions)

        for expected_action, given_action in zipped_list:
            self.assertEquals(expected_action.id, given_action["id"])
            self.assertEquals(expected_action.name, given_action["name"])

    def test_cannot_request_without_login(self):
        response = self.client.get(reverse("actions-list"))
        self.assertEqual(response.status_code, 403)

        error = response.json()

        self.assertTrue("not provided" in error["detail"])


class TestPropertyView(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.distance = 400
        call_command("addactions", "test_action")
        call_command(
            "addproperties", ["average_speed", "average_acceleration", "drowsiness"]
        )

        call_command("filldb", trip_number=10)
        trips = Trip.objects.filter(end_at=None)

        # fill drowsiness
        for trip in trips:
            vehicle_states = trip.vehiclestate_set.all()
            for state in vehicle_states:
                state.driver = DriverState.objects.create(
                    concentrated_on_the_road=random.choice([True, False])
                )
                state.save()

    def test_request_average_speed_property(self):
        self.assertTrue(self.client.login(username="user 0", password="password"))

        neighbors_response = self.client.get(
            reverse("get-neighbors", kwargs={"within_distance_m": self.distance})
        )

        logging.debug(f"response: {neighbors_response.json()}")
        self.assertEqual(neighbors_response.status_code, 200)

        neighbors = neighbors_response.json()
        property_pk = neighbors[0]["trip"]["vehicle"]["allowed_properties"][0]
        trip_pk = neighbors[0]["trip"]["id"]
        property_response = self.client.get(
            reverse(
                "request-property",
                kwargs={
                    "property_pk": property_pk,
                    "trip_pk": trip_pk,
                },
            )
        )

        logging.debug(f"response: {property_response.json()}")
        self.assertEqual(property_response.status_code, 200)

        prop = property_response.json()

        self.assertEqual(prop["property_name"]["name"], "average_speed")
        handler = get_handler()

        expected_property = handler.run_handler(
            NeighborProperty(
                trip_pk=trip_pk,
                property_pk=property_pk,
            )
        )
        self.assertEqual(
            prop["property_value"]["speed__avg"], expected_property["speed__avg"]
        )

    def test_request_average_acc_property(self):
        self.assertTrue(self.client.login(username="user 0", password="password"))

        neighbors_response = self.client.get(
            reverse("get-neighbors", kwargs={"within_distance_m": self.distance})
        )

        logging.debug(f"response: {neighbors_response.json()}")
        self.assertEqual(neighbors_response.status_code, 200)

        neighbors = neighbors_response.json()
        property_pk = neighbors[0]["trip"]["vehicle"]["allowed_properties"][1]
        trip_pk = neighbors[0]["trip"]["id"]
        property_response = self.client.get(
            reverse(
                "request-property",
                kwargs={
                    "property_pk": property_pk,
                    "trip_pk": trip_pk,
                },
            )
        )

        logging.debug(f"response: {property_response.json()}")
        self.assertEqual(property_response.status_code, 200)

        prop = property_response.json()

        self.assertEqual(prop["property_name"]["name"], "average_acceleration")
        handler = get_handler()

        expected_property = handler.run_handler(
            NeighborProperty(
                trip_pk=trip_pk,
                property_pk=property_pk,
            )
        )
        self.assertEqual(
            prop["property_value"]["acceleration__avg"],
            expected_property["acceleration__avg"],
        )

    def test_request_drowsiness_property(self):
        self.assertTrue(self.client.login(username="user 0", password="password"))

        neighbors_response = self.client.get(
            reverse("get-neighbors", kwargs={"within_distance_m": self.distance})
        )

        logging.debug(f"response: {neighbors_response.json()}")
        self.assertEqual(neighbors_response.status_code, 200)

        neighbors = neighbors_response.json()
        property_pk = neighbors[0]["trip"]["vehicle"]["allowed_properties"][2]
        trip_pk = neighbors[0]["trip"]["id"]
        property_response = self.client.get(
            reverse(
                "request-property",
                kwargs={
                    "property_pk": property_pk,
                    "trip_pk": trip_pk,
                },
            )
        )

        logging.debug(f"response: {property_response.json()}")
        self.assertEqual(property_response.status_code, 200)

        prop = property_response.json()

        self.assertEqual(prop["property_name"]["name"], "drowsiness")
        handler = get_handler()

        expected_property = handler.run_handler(
            NeighborProperty(
                trip_pk=trip_pk,
                property_pk=property_pk,
            )
        )
        self.assertEqual(
            prop["property_value"]["drowsiness"],
            expected_property["drowsiness"],
        )

    def test_cannot_request_wrong_property(self):
        self.assertTrue(self.client.login(username="user 0", password="password"))

        neighbors_response = self.client.get(
            reverse("get-neighbors", kwargs={"within_distance_m": self.distance})
        )

        logging.debug(f"neighbors_response: {neighbors_response.json()}")
        self.assertEqual(neighbors_response.status_code, 200)

        neighbors = neighbors_response.json()

        property_pk = neighbors[0]["trip"]["vehicle"]["allowed_properties"][0]
        trip_pk = neighbors[0]["trip"]["id"]
        property_response = self.client.get(
            reverse(
                "request-property",
                kwargs={
                    "property_pk": property_pk + 10,
                    "trip_pk": trip_pk,
                },
            )
        )

        logging.debug(f"property_response: {property_response.json()}")
        self.assertEqual(property_response.status_code, 400)

    def test_cannot_request_property_from_vehicle_out_of_cluster(self):
        self.assertTrue(self.client.login(username="user 0", password="password"))

        neighbors_response = self.client.get(
            reverse("get-neighbors", kwargs={"within_distance_m": self.distance})
        )

        logging.debug(f"neighbors_response: {neighbors_response.json()}")
        self.assertEqual(neighbors_response.status_code, 200)

        neighbors = neighbors_response.json()
        property_pk = neighbors[0]["trip"]["vehicle"]["allowed_properties"][0]

        trip_id = {n["trip"]["id"] for n in neighbors}
        excluded_trip = Trip.objects.exclude(id__in=trip_id).first()
        property_response = self.client.get(
            reverse(
                "request-property",
                kwargs={
                    "property_pk": property_pk,
                    "trip_pk": excluded_trip.id,
                },
            )
        )

        logging.debug(f"property_response: {property_response.json()}")
        self.assertEqual(property_response.status_code, 400)

    def test_get_neighbors(self):
        self.assertTrue(self.client.login(username="user 0", password="password"))

        response = self.client.get(
            reverse("get-neighbors", kwargs={"within_distance_m": self.distance})
        )

        logging.debug(f"response: {response.json()}")

        self.assertEqual(response.status_code, 200)

        cluster = VehicleCluster(CustomUser.objects.get(username="user 0"))
        expected_neighbors = cluster.get_neighbors_states(self.distance)
        neighbors = response.json()

        for e_neighbor, neighbor in zip(expected_neighbors, neighbors):
            self.assertEqual(e_neighbor.trip_id.id, neighbor["trip"]["id"])
            self.assertTrue("vehicle" in neighbor["trip"])
            self.assertTrue("location" in neighbor)

    def test_get_neighbors_with_closed_trip(self):
        # close a trip
        trip = Trip.objects.filter(end_at=None).first()
        trip.end_at = datetime.datetime.now()
        trip.save()
        # get user of the closed trip
        username = trip.user_id.username

        # login under the gotten user
        self.assertTrue(self.client.login(username=username, password="password"))

        # request neighbors
        response = self.client.get(
            reverse("get-neighbors", kwargs={"within_distance_m": self.distance})
        )

        logging.debug(f"response: {response.json()}")
        self.assertEqual(response.status_code, 400)

    def test_get_neighbors_with_empty_states(self):
        # delete states from the trip
        trip = Trip.objects.filter(end_at=None).first()
        trip.vehiclestate_set.all().delete()

        # get user of the closed trip
        username = trip.user_id.username

        # login under the gotten user
        self.assertTrue(self.client.login(username=username, password="password"))

        # request neighbors
        response = self.client.get(
            reverse("get-neighbors", kwargs={"within_distance_m": self.distance})
        )

        logging.debug(f"response: {response.json()}")
        self.assertEqual(response.status_code, 400)
