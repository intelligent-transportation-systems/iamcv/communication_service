import logging

from django.contrib.gis.db.models.functions import Distance
from django.core.management import call_command
from django.test import TestCase
from django.utils import timezone

from api.business_logic.clustering import VehicleCluster
from trip.models import Trip
from user.models import CustomUser

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


class ClusteringTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        call_command("addactions", "test_action")
        call_command("addproperties", "test_property")
        neighbors_location = [
            {"lat": 48.213882, "long": 16.362264},
            {"lat": 48.214267, "long": 16.362312},
            {"lat": 48.213838, "long": 16.362647},
            {"lat": 48.214128, "long": 16.362859},
        ]

        call_command("filldb", trip_number=len(neighbors_location))
        cls.trips = Trip.objects.filter(end_at=None)

        for position, trip in zip(neighbors_location, cls.trips):
            print(f"{trip= }, {position=}")
            latest_state = trip.vehiclestate_set.latest("time_stamp")
            latest_state.time_stamp = timezone.now()
            latest_state.location.position = (
                f"POINT({position['long']} {position['lat']})"
            )
            latest_state.location.save()
            latest_state.save()

    def test_cluster_return_correct_last_user_state(self):
        user = self.trips[0].user_id
        cluster = VehicleCluster(user)
        user_last_position = cluster._get_last_user_location_state()

        expected_user_position = (
            user.trip_set.get(end_at=None)
            .vehiclestate_set.latest("time_stamp")
            .location
        )

        self.assertEqual(user_last_position, expected_user_position)

    def test_return_correct_neighbors_location(self):
        user = CustomUser.objects.all().first()
        cluster = VehicleCluster(user)
        distance = 50  # m
        last_user_location = cluster._get_last_user_location_state()
        last_states_slice = cluster._get_last_slice_of_states(last_user_location)
        neighbors_states = cluster._compute_neighbors_locations(
            last_states_slice, last_user_location, distance
        )
        self.assertTrue(neighbors_states)
        self.assertTrue(last_user_location.vehiclestate not in neighbors_states)

        for neighbor_location in neighbors_states.annotate(
            distance=Distance("location__position", last_user_location.position)
        ):
            self.assertTrue(neighbor_location.distance.m <= distance)

    def test_return_correct_distance_between_neighbors(self):
        user = self.trips[0].user_id
        cluster = VehicleCluster(user)

        last_user_location = cluster._get_last_user_location_state()
        distance = 50  # m

        neighbors_states = cluster.get_neighbors_states(within_m=distance)
        self.assertTrue(neighbors_states)

        for neighbor_location in neighbors_states.annotate(
            distance=Distance("location__position", last_user_location.position)
        ):
            self.assertTrue(neighbor_location.distance.m <= distance)

    def test_neighbors_does_not_includes_user(self):
        user = self.trips[0].user_id
        cluster = VehicleCluster(user)

        last_user_location = cluster._get_last_user_location_state()
        distance = 50  # m

        neighbors_states = cluster.get_neighbors_states(within_m=distance)
        self.assertTrue(neighbors_states)

        self.assertTrue(last_user_location.vehiclestate not in neighbors_states)

    def test_return_cluster_uses_last_states_of_neighbors(self):
        user = self.trips[0].user_id
        cluster = VehicleCluster(user)

        distance = 50  # m

        neighbors_states = cluster.get_neighbors_states(within_m=distance)
        self.assertTrue(neighbors_states)

        for state in neighbors_states:
            cluster = VehicleCluster(state.trip_id.user_id)
            latest_state = cluster._get_last_user_location_state()
            print(f"{latest_state.id=}, {state.id=}")
            self.assertEqual(latest_state.id, state.id)

    def test_cache(self):
        user = self.trips[0].user_id
        cluster = VehicleCluster(user)

        distance = 50  # m

        neighbors_states = cluster.get_neighbors_states(within_m=distance)
        cache = cluster.get_cached_neighbors_states()
        self.assertTrue(neighbors_states)

        for c_n, n in zip(cache, neighbors_states):
            self.assertEqual(c_n.id, n.id)
