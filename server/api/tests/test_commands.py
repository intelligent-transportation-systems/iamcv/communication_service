from django.core.management import base, call_command
from django.test import TestCase

from api.models import Action, Property
from trip.models import Trip
from user.models import CustomUser
from vehicle.models import (
    Vehicle,
    VehicleBody,
    VehicleManufacture,
    VehicleModel,
    VehicleModelYear,
)
from vehicle_state.models import ControlState, EngineState, LocationState, VehicleState


class AddActionsCommandTest(TestCase):
    # test
    def test_can_add_actions(self):
        actions_list = ["test-command-1", "test-command-2", "test-command-3"]
        call_command("addactions", actions_list)
        actions = list(Action.objects.values_list("name", flat=True))

        self.assertEqual(actions_list, actions)

    def test_cannot_add_empty_actions(self):
        actions_list = []
        with self.assertRaises(base.CommandError):
            call_command("addactions", actions_list)


class AddPropertiesCommandTest(TestCase):
    def test_can_add_properties(self):
        properties_list = ["test-property-1", "test-property-2", "test-property-3"]
        call_command("addproperties", properties_list)
        properties = list(Property.objects.values_list("name", flat=True))

        self.assertEqual(properties_list, properties)

    def test_cannot_add_empty_actions(self):
        properties_list = []
        with self.assertRaises(base.CommandError):
            call_command("addproperties", properties_list)


class CleanDatabaseCommandTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = CustomUser.objects.create_user(username="user", password="password")

        call_command("addactions", ["testaction1", "testaction2"])
        call_command("addproperties", ["average_speed", "average_acceleration"])
        call_command("addvehiclemodels", 2)

        call_command("createvehicle", user_id=user.id)
        call_command("createtrip", user_id=user.id)
        trip = user.trip_set.get(end_at=None)
        call_command("addvehiclestates", trip_id=trip.id, dataset_trip_id=1)

    def test_cleandatabase(self):
        call_command("cleandatabase")
        self.assertTrue(not Action.objects.all().exists())
        self.assertTrue(not Property.objects.all().exists())
        self.assertTrue(not Trip.objects.all().exists())
        self.assertTrue(not Vehicle.objects.all().exists())
        self.assertTrue(not VehicleBody.objects.all().exists())
        self.assertTrue(not VehicleManufacture.objects.all().exists())
        self.assertTrue(not VehicleModel.objects.all().exists())
        self.assertTrue(not VehicleModelYear.objects.all().exists())
        self.assertTrue(not VehicleState.objects.all().exists())
        self.assertTrue(not LocationState.objects.all().exists())
        self.assertTrue(not EngineState.objects.all().exists())
        self.assertTrue(not ControlState.objects.all().exists())


class FillDbCommandTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        properties_list = ["test-property-1", "test-property-2", "test-property-3"]
        call_command("addproperties", properties_list)

        actions_list = ["test-command-1", "test-command-2", "test-command-3"]
        call_command("addactions", actions_list)

    def test_fill_db(self):
        trip_number = 2
        call_command("filldb", trip_number=trip_number)

        self.assertEqual(CustomUser.objects.all().count(), trip_number)
        self.assertEqual(Vehicle.objects.all().count(), trip_number)

        trips = Trip.objects.filter(end_at=None).all()
        self.assertEqual(trips.count(), trip_number)

        for trip in trips:
            self.assertTrue(trip.vehiclestate_set.count() != 0)
