from django.core.management import call_command
from django.test import TestCase

from trip.models import Trip
from user.business_logic.account import remove_user_data
from user.models import CustomUser
from vehicle.models import Vehicle
from vehicle_state.models import LocationState, VehicleState


class RemoveUserTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        call_command("addactions", "test_action")
        call_command("addproperties", "test_property")
        call_command("filldb", trip_number=1)
        cls.user_model = CustomUser.objects.first()

    def test_remove_user_account(self):
        user_id = self.user_model.id
        vehicle_ids = self.user_model.vehicle_set.values_list("id", flat=True)
        trip_ids = self.user_model.trip_set.values_list("id", flat=True)
        vehicle_state_ids = VehicleState.objects.filter(
            trip_id__in=trip_ids
        ).values_list("id", flat=True)
        location_state_ids = LocationState.objects.filter(
            id__in=vehicle_state_ids
        ).values_list("id", flat=True)

        self.assertTrue(CustomUser.objects.filter(id=user_id).exists())
        self.assertTrue(Vehicle.objects.filter(id__in=vehicle_ids).exists())
        self.assertTrue(Trip.objects.filter(id__in=trip_ids).exists())
        self.assertTrue(VehicleState.objects.filter(id__in=vehicle_state_ids).exists())

        self.assertTrue(
            LocationState.objects.filter(id__in=location_state_ids).exists()
        )

        remove_user_data(self.user_model)

        self.assertTrue(not CustomUser.objects.filter(id=user_id).exists())
        self.assertTrue(not Vehicle.objects.filter(id__in=vehicle_ids).exists())
        self.assertTrue(not Trip.objects.filter(id__in=trip_ids).exists())
        self.assertTrue(
            not VehicleState.objects.filter(id__in=vehicle_state_ids).exists()
        )

        self.assertTrue(
            not LocationState.objects.filter(id__in=location_state_ids).exists()
        )
