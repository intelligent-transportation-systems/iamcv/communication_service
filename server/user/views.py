from rest_framework.generics import DestroyAPIView

from .business_logic.account import remove_user_data
from .models import CustomUser


class RemoveAccountView(DestroyAPIView):
    def get_object(self):
        return self.request.user

    def perform_destroy(self, user: CustomUser):
        remove_user_data(CustomUser.objects.get(id=user.id))
