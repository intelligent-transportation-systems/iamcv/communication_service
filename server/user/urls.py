from django.urls import path

from .views import RemoveAccountView

urlpatterns = [
    path("remove-account/", RemoveAccountView.as_view(), name="remove-account"),
]
