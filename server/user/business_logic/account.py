from trip.models import Trip
from user.models import CustomUser
from vehicle.models import Vehicle
from vehicle_state.models import ControlState, EngineState, LocationState, VehicleState


def remove_user_data(user: CustomUser):
    user_id = user.id
    vehicle_ids = user.vehicle_set.values_list("id", flat=True)
    trip_ids = user.trip_set.values_list("id", flat=True)
    vehicle_state_ids = VehicleState.objects.filter(trip_id__in=trip_ids).values_list(
        "id", flat=True
    )
    location_state_ids = LocationState.objects.filter(
        id__in=vehicle_state_ids
    ).values_list("id", flat=True)
    engine_state_ids = EngineState.objects.filter(id__in=vehicle_state_ids).values_list(
        "id", flat=True
    )
    control_state_ids = ControlState.objects.filter(
        id__in=vehicle_state_ids
    ).values_list("id", flat=True)

    LocationState.objects.filter(id__in=location_state_ids).delete()
    EngineState.objects.filter(id__in=engine_state_ids).delete()
    ControlState.objects.filter(id__in=control_state_ids).delete()

    VehicleState.objects.filter(id__in=vehicle_state_ids).delete()

    Trip.objects.filter(id__in=trip_ids).delete()

    Vehicle.objects.filter(id__in=vehicle_ids).delete()

    CustomUser.objects.filter(id=user_id).delete()
