import logging

from django.core.management import call_command
from django.test import TestCase

from trip.models import Trip
from user.models import CustomUser
from vehicle.models import Vehicle

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


class CreateTripCommandTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = CustomUser.objects.create_user(
            username="testuser", password="testpassword"
        )

        cls.user2 = CustomUser.objects.create_user(
            username="testuser2", password="testpassword"
        )

        call_command("addactions", ["testaction1", "testaction2"])
        call_command("addproperties", ["testproperty1", "testproperty2"])
        call_command("addvehiclemodels", 2)

        call_command("createvehicle", user_id=cls.user.id)
        call_command("createvehicle", user_id=cls.user2.id)

    def test_create_trip(self):
        call_command("createtrip")
        trip = Trip.objects.first()
        vehicle = Vehicle.objects.filter(user_id=self.user).first()

        self.assertEqual(trip.user_id, self.user)
        self.assertEqual(trip.vehicle, vehicle)
        self.assertEqual(trip.end_at, None)

    def test_cannot_have_several_active_trips(self):
        call_command("createtrip")
        with self.assertRaises(ValueError):
            call_command("createtrip")

    def test_vehicle_option(self):
        vehicle = Vehicle.objects.filter(user_id=self.user).last()
        call_command("createtrip", vehicle_id=vehicle.id)
        trip = Trip.objects.first()

        self.assertEqual(trip.vehicle, vehicle)

    def test_cannot_add_wrong_vehicle(self):
        vehicle_id = max(list(Vehicle.objects.values_list("id", flat=True))) + 1

        with self.assertRaises(ValueError):
            call_command("createtrip", vehicle_id=vehicle_id)

    def test_user_option(self):
        call_command("createtrip", user_id=self.user.id)
        call_command("createtrip", user_id=self.user2.id)

        trip1 = Trip.objects.get(user_id=self.user.id)
        trip2 = Trip.objects.get(user_id=self.user2.id)

        self.assertEqual(trip1.user_id, self.user)
        self.assertEqual(trip2.user_id, self.user2)

    def test_cannot_add_wrong_user_option(self):
        user_id = CustomUser.objects.count() + 2

        with self.assertRaises(ValueError):
            call_command("createtrip", vehicle_id=user_id)


class CloseTripCommandTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = CustomUser.objects.create_user(
            username="testuser", password="testpassword"
        )

        cls.user2 = CustomUser.objects.create_user(
            username="testuser2", password="testpassword"
        )

        call_command("addactions", ["testaction1", "testaction2"])
        call_command("addproperties", ["testproperty1", "testproperty2"])
        call_command("addvehiclemodels", 2)

        call_command("createvehicle", user_id=cls.user.id)
        call_command("createvehicle", user_id=cls.user2.id)

        call_command("createtrip", user_id=cls.user.id)
        call_command("createtrip", user_id=cls.user2.id)

    def test_close_trip(self):
        trip = Trip.objects.filter(user_id=self.user).first()
        self.assertEquals(trip.end_at, None)

        call_command("closetrip", self.user.id)

        trip = Trip.objects.filter(user_id=self.user).first()
        self.assertTrue(trip.end_at is not None)
