import logging
from typing import Type, Union

from dateutil import parser
from django.core.management import call_command
from django.test import TestCase
from django.urls import reverse

from api.models import Action, Property
from trip.models import Trip
from user.models import CustomUser
from vehicle.models import Vehicle

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

one_field_models = Type[Union[Action, Property]]


class TestTripView(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.username = "testuser"
        cls.username2 = "testuser2"
        cls.password = "testpassword"
        cls.user = CustomUser.objects.create_user(
            username=cls.username, password=cls.password
        )
        cls.user2 = CustomUser.objects.create_user(
            username=cls.username2, password=cls.password
        )

        call_command("addvehiclemodels", 10)
        call_command("addproperties", ["test_prop1", "test_prop2"])
        call_command("addactions", ["test_action1", "test_action2"])
        call_command("createvehicle", user_id=cls.user.id)
        call_command("createvehicle", user_id=cls.user.id)
        call_command("createvehicle", user_id=cls.user2.id)

        cls.vehicle = Vehicle.objects.filter(user_id=cls.user).first()
        cls.vehicle2 = Vehicle.objects.filter(user_id=cls.user2).first()
        cls.vehicle3 = Vehicle.objects.filter(user_id=cls.user)[1]

    def setUp(self):
        self.assertTrue(
            self.client.login(username=self.username, password=self.password)
        )

    def test_create_trip(self):
        response = self.client.post(
            reverse("open-trip"),
            data={
                "vehicle": self.vehicle.id,
            },
            content_type="application/json",
        )
        logger.debug(f"response: {response.json()}")

        self.assertEqual(response.status_code, 201)
        self.assertTrue(Trip.objects.filter(vehicle=self.vehicle).exists())

    def test_cannot_add_wrong_vehicle(self):
        response = self.client.post(
            reverse("open-trip"),
            data={
                "vehicle": self.vehicle2.id,
            },
            content_type="application/json",
        )
        logger.debug(f"response: {response.json()}")

        self.assertEqual(response.status_code, 400)

    def test_cannot_add_null_vehicle(self):
        response = self.client.post(
            reverse("open-trip"),
            data={
                "vehicle": None,
            },
            content_type="application/json",
        )
        logger.debug(f"response: {response.json()}")

        self.assertEqual(response.status_code, 400)

    def test_cannot_have_several_active_trips(self):
        self.test_create_trip()
        response = self.client.post(
            reverse("open-trip"),
            content_type="application/json",
            data={
                "vehicle": self.vehicle.id,
            },
        )

        logger.debug(f"response: {response.json()}")
        self.assertEqual(response.status_code, 400)

    def test_can_close_trip(self):
        self.test_create_trip()
        trip = Trip.objects.filter(vehicle=self.vehicle, user_id=self.user).get(
            end_at=None
        )
        response = self.client.get(
            reverse("close-trip"), content_type="application/json"
        )
        logger.debug(f"response: {response.json()}")
        self.assertEqual(response.status_code, 200)

        updated_trip = Trip.objects.get(id=trip.id)
        data = response.json()
        self.assertEqual(data["vehicle"], trip.vehicle.id)
        self.assertTrue(updated_trip.end_at is not None)
        self.assertEqual(parser.isoparse(data["end_at"]), updated_trip.end_at)

    def test_can_retrieve_trips_list(self):
        self.test_create_trip()

        response = self.client.get(reverse("list-trips"))
        logger.debug(f"response: {response.json()}")
        self.assertEqual(response.status_code, 200)

        data = response.json()
        self.assertEqual(len(data), 1)

        self.assertEqual(data[0]["vehicle"]["id"], self.vehicle.id)
        self.assertEqual(data[0]["vehicle"]["plate_number"], self.vehicle.plate_number)
        self.assertEqual(data[0]["vehicle"]["vin"], self.vehicle.vin)
        self.assertEqual(data[0]["end_at"], None)

    def test_user_cannot_see_foreign_trips_in_list(self):
        self.test_create_trip()
        self.client.logout()
        self.assertTrue(
            self.client.login(username=self.username2, password=self.password)
        )

        response = self.client.get(reverse("list-trips"))

        logger.debug(f"response: {response.json()}")
        self.assertEqual(response.status_code, 200)

        data = response.json()
        self.assertEqual(len(data), 0)
