from django.db import models
from user.models import CustomUser
from vehicle.models import Vehicle


class Trip(models.Model):
    user_id = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    vehicle = models.ForeignKey(
        Vehicle, null=True, blank=False, on_delete=models.SET_NULL, db_index=True
    )
    start_at = models.DateTimeField(auto_now_add=True, db_index=True)
    end_at = models.DateTimeField(null=True, blank=True, db_index=True)

    def __str__(self):
        return f"{self.user_id.username}'s trip ({str(self.start_at)}:{str(self.end_at)}) id {self.id}"

    # ToDo: think about this fields, but for now we can obtain them runtime
    # average_speed = models.FloatField(default=0)
    # maximum_speed = models.FloatField(default=0)
    # mobility_distance = models.FloatField(default=0)
    # start_position = models.ForeignKey(Location, on_delete=models.CASCADE)
    # end_position = models.ForeignKey(Location, on_delete=models.CASCADE)
    # fft_speed =
    # fft_acceleration =
    # speed_distribution =
    # acceleration_distribution =
