import datetime

from rest_framework import generics
from rest_framework.response import Response

from RUCS.filters import UserFilter

from .models import Trip
from .serializers import ListTripSerializer, TripSerializer


class OpenTripView(generics.CreateAPIView):
    queryset = Trip.objects.all()
    serializer_class = TripSerializer

    def perform_create(self, serializer):
        serializer.save(user_id=self.request.user)


class CloseTripView(generics.RetrieveAPIView):
    serializer_class = TripSerializer

    def get_object(self):
        return Trip.objects.filter(user_id=self.request.user).get(end_at=None)

    def get_end_at_data(self):
        return {"end_at": datetime.datetime.now()}

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        data = self.get_end_at_data()

        serializer = self.get_serializer(instance, data=data, partial=True)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        return Response(serializer.data)

    def perform_update(self, serializer: TripSerializer):
        serializer.save(user_id=self.request.user)


class ListTripView(generics.ListAPIView):
    queryset = Trip.objects.all()
    serializer_class = ListTripSerializer
    filterset_class = UserFilter
