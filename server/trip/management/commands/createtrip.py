import logging

from django.core.management.base import BaseCommand
from django.db import transaction

from trip.models import Trip
from user.models import CustomUser
from vehicle.models import Vehicle

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


class Command(BaseCommand):
    help = "create trip for a user using one of his vehicles"

    def add_arguments(self, parser):
        parser.add_argument(
            "--vehicle_id",
            dest="vehicle",
            type=int,
            help="vehicle pk number",
            default=None,
        )
        parser.add_argument(
            "--user_id",
            dest="user",
            choices=list(CustomUser.objects.values_list("id", flat=True)),
            type=int,
            help="vehicle pk number",
            default=list(CustomUser.objects.values_list("id", flat=True))[0],
        )

    def _get_vehicle(self, vehicle_id: int, user: CustomUser) -> Vehicle:
        if vehicle_id is not None:
            if vehicle_id in list(user.vehicle_set.values_list("id", flat=True)):
                return Vehicle.objects.get(id=vehicle_id)

            else:
                raise ValueError(
                    f"User {user.id} does not have the vehicle {vehicle_id}"
                )
        else:
            vehicles = user.vehicle_set.all()
            if not vehicles:
                raise ValueError(f"User {user.id} does not have vehicles")

            return vehicles[0]

    def handle(self, *args, **options):
        with transaction.atomic():
            user = CustomUser.objects.get(id=options["user"])
            vehicle = self._get_vehicle(options["vehicle"], user)

            logger.debug(f"{vehicle=}, {user=}")

            if Trip.objects.filter(end_at=None, user_id=user).count() != 0:
                raise ValueError(
                    f"User {user.id} already has opened trip."
                    "If you want to create new one please first closed opened one."
                )

            trip = Trip(user_id=user, vehicle_id=vehicle.id)
            trip.full_clean()
            trip.save()
