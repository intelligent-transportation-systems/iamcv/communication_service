import logging

from django.core.management.base import BaseCommand
from django.db import transaction
from django.utils import timezone

from trip.models import Trip
from user.models import CustomUser

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


class Command(BaseCommand):
    help = "close trip for a given user"

    def add_arguments(self, parser):
        parser.add_argument(
            "user_id",
            choices=list(CustomUser.objects.values_list("id", flat=True)),
            type=int,
            help="user pk number",
        )

    def handle(self, *args, **options):
        with transaction.atomic():
            trips = Trip.objects.filter(user_id=options["user_id"], end_at=None)

            for trip in trips:
                trip.end_at = timezone.now()
                trip.save()
