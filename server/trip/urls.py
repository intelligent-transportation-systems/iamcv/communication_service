from django.urls import path

from .views import CloseTripView, ListTripView, OpenTripView

urlpatterns = [
    path("open", OpenTripView.as_view(), name="open-trip"),
    path("close", CloseTripView.as_view(), name="close-trip"),
    path("list", ListTripView.as_view(), name="list-trips"),
]
