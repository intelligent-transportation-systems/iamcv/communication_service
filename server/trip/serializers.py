from typing import Any

from rest_framework import serializers

from vehicle.models import Vehicle
from vehicle.serializers import NeighborVehicleSerializer, VehicleListSerializer

from .models import Trip


class TripSerializer(serializers.ModelSerializer):
    vehicle: Any = serializers.PrimaryKeyRelatedField(
        queryset=Vehicle.objects.all(), allow_null=False
    )

    def validate_vehicle(self, vehicle):
        if vehicle not in self.context["request"].user.vehicle_set.all():
            raise serializers.ValidationError(f"User does not have vehicle {vehicle}")

        return vehicle

    def validate(self, data):
        if self.context["request"].method == "POST":
            if self.context["request"].user.trip_set.filter(end_at=None).count() > 0:
                raise serializers.ValidationError(
                    "You cannot create a trip while you have opened trip"
                )

        return super(TripSerializer, self).validate(data)

    class Meta:
        model = Trip
        fields = ["id", "vehicle", "end_at"]


class ListTripSerializer(TripSerializer):
    vehicle = VehicleListSerializer()


class NeighborTripSerializer(serializers.ModelSerializer):
    vehicle = NeighborVehicleSerializer()
    # ToDo: only for the debug purpose
    user_id: serializers.SlugRelatedField = serializers.SlugRelatedField(
        read_only=True, slug_field="username"
    )

    class Meta:
        model = Trip
        fields = ["id", "vehicle", "user_id"]
